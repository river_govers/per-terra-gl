#version 410

in vec3 vViewDir;

out vec4 fragColor;

uniform samplerCube cubeMap;

void main()
{
    fragColor = texture(cubeMap, vViewDir);
}