#version 410

out vec2 vTexCoords;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 world;

void main()
{
    mat4 camTransform = inverse(view);

    // vec4 pos = vec4(camTransform[3].xyz + ((world[3] * 3.0f) + vec4(((gl_VertexID & 1) << 2) - 1, (gl_VertexID & 2) * 2 - 1, 0.0f, 1.0f)).xyz, 1.0f);

    // vec4 pos = vec4(camTransform[3].xyz + (world * vec4(((gl_VertexID & 1) << 2) - 1, (gl_VertexID & 2) * 2 - 1, 0.0f, 1.0f)).xyz, 1.0f);
    vec4 pos = world * vec4(((gl_VertexID & 1) << 2) - 1, (gl_VertexID & 2) * 2 - 1, 0.0f, 1.0f);
    // vec4 pos = camTransform[3] + (world * vec4(((gl_VertexID & 1) << 2) - 1, (gl_VertexID & 2) * 2 - 1, 0.0f, 1.0f));
    // pos.w = 1.0f;

    //vec4 pos = vec4(camTransform[3].xyz + vec3(((gl_VertexID & 1) << 2) - 1, (gl_VertexID & 2) * 2 - 1, 0.0f), 1.0f);

    // Position normalized
    //vec3 zAxis = normalize(camTransform[3].xyz - pos.xyz);
    vec3 zAxis = normalize(pos.xyz);
    vec3 xAxis = cross(camTransform[1].xyz, zAxis);
    vec3 yAxis = cross(zAxis, xAxis);
    mat3 billboard = mat3(xAxis, yAxis, zAxis);

    //gl_Position = projection * view * vec4(billboard * pos.xyz, 1.0f);
    gl_Position = projection * vec4(mat3(view) * billboard * pos.xyz, 1.0f);

    vTexCoords = vec2(((gl_VertexID & 1) << 2) - 1, (gl_VertexID & 2) * 2 - 1);
}