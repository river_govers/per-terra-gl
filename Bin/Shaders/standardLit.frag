#version 410

#define MAX_LIGHTS 16

struct Light
{
    int type;
    mat4 matrix;
    vec3 pos;
    vec3 dir;
    vec4 color;
    sampler2D shadowMap;
};

in vec3 vNormal;
in vec3 vPosition;
in vec4 vTangent;
in vec4 vBiTangent;
in vec2 vTexCoord;
flat in vec3 vCamPos;
in mat3 vTangentBiTangentNormal;
in vec4 vShadowPosition; 

out vec4 fragColor;

uniform sampler2D diffuse;
uniform sampler2D normalMap;
uniform sampler2D specularMap;

uniform int lightCount;
uniform Light lights[MAX_LIGHTS];

void main()
{
    float d;

    mat3 TBN;

    TBN[0] = vTangent.xyz;
    TBN[1] = vBiTangent.xyz;
    TBN[2] = vNormal;

    vec3 normal = TBN * (texture(normalMap, vTexCoord) * 2.0f - 1.0f).xyz;

    vec4 specular = vec4(0.0f);
    vec4 diffuseCol = vec4(0.0f);

    for (int i = 0; i < lightCount; ++i)
    {
        switch (lights[i].type)
        {
        case 1:
            {
               if (texture(lights[i].shadowMap, vShadowPosition.xy).r < vShadowPosition.z - 0.025f)
               {
                   d = 0.0f;
               }
               else
               {
                    d = max(0, dot(normalize(normal), lights[i].dir)); 
               }
            
                diffuseCol += vec4(0.25f) * lights[i].color * d;
                float specTerm = pow(max(0.0f, dot(reflect(lights[i].dir, normal), normalize(vPosition - vCamPos))), 9.0f);
                specular += texture(specularMap, vTexCoord.xy) * specTerm;
                break;
            }
        case 2:
            {
                break;
            }
        }
    }

    vec4 texColor = texture(diffuse, vTexCoord.xy);

    //fragColor = vec4((texColor + specLight).xyz, 1.0f);
    fragColor = vec4((texColor + diffuseCol + specular).xyz, 1.0f);
}