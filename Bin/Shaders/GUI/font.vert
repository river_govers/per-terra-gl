#version 410

layout(location = 0) in vec2 position;
layout(location = 1) in vec2 texCoord;

out vec2 vTexCoord;

uniform mat4 projection;

void main()
{
	//gl_Position = mat4(0.00104166666666666666666666666667f, 0.0f, 0.0f, 0.0f,
	//					0.0f, 0.00185185185185185185185185185185, 0.0f, 0.0f,
	//					0.0f, 0.0f, -1.0f, 0.0f,
	//					0.0f, 0.0f, 0.0f, 1.0f) * vec4(position.xy, 0.0f, 1.0f);
	gl_Position = projection * vec4(position.xy, 0.0f, 1.0f);
	//gl_Position = vec4(position.xy, 0.0f, 1.0f);
    vTexCoord = texCoord;
}