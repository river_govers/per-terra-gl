#version 410

in vec2 vTexCoord;

uniform sampler2D font;
uniform vec4 fontColor;

out vec4 color;

void main()
{
	color = vec4(1.0f, 1.0f, 1.0f, texture(font, vTexCoord).r) * fontColor;
	//color = vec4(1.0f);
	//color = vec4(texture2D(font, vTexCoord).r);
	//color = fontColor;
	//color = vec4(vTexCoord.xy, 0.0f, 1.0f);
}