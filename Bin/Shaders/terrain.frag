#version 410

#define MAX_LIGHTS 16

struct Light
{
    int type;
    mat4 matrix;
    vec3 pos;
    vec3 dir;
    vec4 color;
    sampler2D shadowMap;
};

in vec2 gTexCoord; 
in vec3 gShadowPosition;
in vec3 gPosition;
in vec3 gShadowPos;
flat in vec3 gCamPos;
in vec3 gNormal;

out vec4 fragColor; 

uniform sampler2D diffuse; 

uniform int lightCount;
uniform Light lights[MAX_LIGHTS];

void main() 
{ 
    vec4 diffuseCol = vec4(0.0f);
    vec4 specular = vec4(0.0f);

	float d;

    vec4 texColor = texture(diffuse, gTexCoord.xy);

	for (int i = 0; i < lightCount; ++i)
    {
        switch (lights[i].type)
        {
        case 1:
            {
               vec4 shadowCoord = vec4(gShadowPosition.xyz, 1.0f);
              
               if (texture(lights[i].shadowMap, shadowCoord.xy).r < shadowCoord.z - 0.025f)
               {
                    d = 0.0f;
               }
               else
               {
                    d = max(0.0f, dot(normalize(gNormal), -lights[i].dir)); 
               }

               diffuseCol += vec4(0.25f) * lights[i].color * d;
               float specTerm = pow(max(0.0f, dot(reflect(lights[i].dir, gNormal), normalize(gPosition - gCamPos))), 5.0f);
               specular += vec4(1.0f) * specTerm;

               break;
            }
        case 2:
            {
                break;
            }
        }
    }

    fragColor = vec4((texColor + diffuseCol + specular).xyz, 1.0f);
    //fragColor = vec4(gNormal, 1.0f);
}