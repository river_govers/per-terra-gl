#version 410

in vec2 vTexCoords;

out vec4 fragColor;

void main()
{
    fragColor = vec4(vTexCoords, 0.0f, 1.0f);
}