#version 410

layout(location = 0) in vec4 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texcoord;

uniform mat4 lightMatrix;
uniform mat4 world;

void main()
{
    gl_Position = lightMatrix * world * position;
}