#version 410

in vec4 gColour;
in vec2 gTexCoord;
in float vTime;

uniform sampler2D diffuse;

out vec4 color;

void main()
{
    vec4 colorTex = texture(diffuse, gTexCoord);

    if (colorTex.a <= 0.5f)
    {
        discard;
    }

    color = gColour * colorTex;

    //color = vec4(1.0f);
}