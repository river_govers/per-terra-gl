#version 410

#define MAX_LIGHTS 16

struct Light
{
    int type;
    mat4 matrix;
    vec3 pos;
    vec3 dir;
    vec4 color;
    sampler2D shadowMap;
};

uniform Light lights[MAX_LIGHTS];

layout(location = 0) in vec4 position; 
layout(location = 1) in vec2 texCoord; 

out vec2 vTexCoord; 
out vec3 vShadowPosition;
out vec3 vPosition;
flat out vec3 vCamPos;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 world;

void main() 
{ 
	vTexCoord = texCoord;
    vShadowPosition = (lights[0].matrix * world * position).xyz;
    vPosition = (world * position).xyz;

	vCamPos = inverse(view)[3].xyz;

	gl_Position = ((projection * view) * world) * position;
}