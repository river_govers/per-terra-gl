#version 410

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in vec3 vShadowPosition[];
in vec3 vPosition[];
in vec2 vTexCoord[];
flat in vec3 vCamPos[];

out vec3 gNormal;
out vec3 gShadowPosition;
out vec3 gPosition;
out vec2 gTexCoord;
flat out vec3 gCamPos;

void main()
{
    vec3 vertexA = vPosition[0];
    vec3 vertexB = vPosition[1];
    vec3 vertexC = vPosition[2];

    vec3 v1 = vertexC - vertexA;
    vec3 v2 = vertexB - vertexA;

    gl_Position = gl_in[0].gl_Position;
    gPosition = vPosition[0];
    gNormal = normalize(cross(v2, v1));
    gShadowPosition = vShadowPosition[0];
    gTexCoord = vTexCoord[0];
    gCamPos = vCamPos[0];
    EmitVertex();

    v1 = vertexA - vertexB;
    v2 = vertexC - vertexB;

    gl_Position = gl_in[1].gl_Position;
    gPosition = vPosition[1];
    gNormal = normalize(cross(v2, v1));
    gShadowPosition = vShadowPosition[1];
    gTexCoord = vTexCoord[1];
    gCamPos = vCamPos[1];
    EmitVertex();

    v1 = vertexB - vertexC;
    v2 = vertexA - vertexC;

    gl_Position = gl_in[2].gl_Position;
    gPosition = vPosition[2];
    gNormal = normalize(cross(v2, v1));
    gShadowPosition = vShadowPosition[2];
    gTexCoord = vTexCoord[2];
    gCamPos = vCamPos[2];
    EmitVertex();
}