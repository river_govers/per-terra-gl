#version 410

out vec3 vViewDir;

uniform mat4 invView;
uniform mat4 invProjection;

void main()
{
    gl_Position = vec4(((gl_VertexID & 1) << 2) - 1, (gl_VertexID & 2) * 2 - 1, 0.0f, 1.0f);
    vViewDir = mat3(invView) * (invProjection * gl_Position).xyz;
}