#version 410

layout(location = 0) in vec4 position; 
layout(location = 2) in vec2 texCoord; 

out vec2 vTexCoord; 

uniform mat4 projection;
uniform mat4 view;
uniform mat4 world;

void main() 
{ 
	vTexCoord = texCoord;
	gl_Position = ((projection * view) * world) * position;
}
