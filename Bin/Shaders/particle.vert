#version 410

layout(location = 0) in vec4 position;
layout(location = 1) in vec3 velocity;
layout(location = 2) in vec4 color;
layout(location = 3) in vec4 endColor;
layout(location = 4) in float size;
layout(location = 5) in float endSize;
layout(location = 6) in float spawnTime;
layout(location = 7) in float endTime;

uniform float timePassed;

out vec4 vColour;
out float vSize;
out float vTime;

void main()
{
    float lifeTime = timePassed - spawnTime;
    float lifeSpan = endTime - spawnTime;

    gl_Position = vec4(position.xyz + (velocity * lifeTime), 1.0f);
    vSize = max(0.0f,mix(size, endSize, lifeTime / lifeSpan));
    vColour = mix(color, endColor, lifeTime / lifeSpan);
}