#version 410

#define MAX_LIGHTS 16

struct Light
{
    int type;
    mat4 matrix;
    vec3 pos;
    vec3 dir;
    vec4 color;
    sampler2D shadowMap;
};

layout(location = 0) in vec4 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;
layout(location = 3) in vec4 tangent;
layout(location = 4) in vec4 biTangent;

out vec3 vNormal;
out vec4 vTangent;
out vec4 vBiTangent;
out vec4 vShadowPosition;
out vec3 vPostion;
out vec2 vTexCoord;
flat out vec3 vCamPos;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 world;

uniform Light lights[MAX_LIGHTS];

void main()
{
    vNormal = normal;
    vTexCoord = texCoord;

    gl_Position = ((projection * view) * world) * position;

    vShadowPosition = lights[0].matrix * world * position;
    vPostion = (world * position).xyz;

    vNormal = normal;
    vTangent = tangent;
    vBiTangent = biTangent;

    vCamPos = view[3].xyz;
}