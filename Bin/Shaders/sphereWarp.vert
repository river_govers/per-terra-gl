#version 410

layout(location=0) in vec4 position; 
layout(location=1) in vec3 normal;
layout(location=2) in vec2 texCoord; 

out vec2 vTexCoord; 
out vec3 vNormal;

uniform mat4 projectionViewWorldMatrix; 
uniform float time;

void main() 
{ 
	vTexCoord = texCoord;
	vNormal = normal;
	gl_Position = (projectionViewWorldMatrix * position) + (vec4(normal, 1.0f) * (2.0f + sin(time + position.x + position.y + position.z)));
}