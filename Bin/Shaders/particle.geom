#version 410

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

in float vSize[];
in vec4 vColour[];

uniform mat4 projection;
uniform mat4 view;

out vec4 gColour;
out vec2 gTexCoord;

void main()
{
    float halfSize = vSize[0] * 0.5f;

    vec4 pos = gl_in[0].gl_Position;

    vec3 topLeft = vec3(-halfSize, -halfSize, 0.0f);
    vec3 topRight = vec3(halfSize, -halfSize, 0.0f);
    vec3 bottomLeft = vec3(-halfSize, halfSize, 0.0f);
    vec3 bottomRight = vec3(halfSize, halfSize, 0.0f);

    mat4 camTransform = inverse(view);

    vec3 zAxis = normalize(camTransform[3].xyz - pos.xyz);
    vec3 xAxis = cross(camTransform[1].xyz, zAxis);
    vec3 yAxis = cross(zAxis, xAxis);
    mat3 billboard = mat3(xAxis, yAxis, zAxis);

	gColour = vColour[0];

    gl_Position = (projection * view) * vec4(billboard * topLeft + pos.xyz, 1.0f);
    gTexCoord = vec2(0.0f, 0.0f);
    EmitVertex();

    gl_Position = (projection * view) * vec4(billboard * topRight + pos.xyz, 1.0f);
    gTexCoord = vec2(1.0f, 0.0f);
    EmitVertex();

    gl_Position = (projection * view) * vec4(billboard * bottomLeft + pos.xyz, 1.0f);
    gTexCoord = vec2(0.0f, 1.0f);
    EmitVertex();

    gl_Position = (projection * view) * vec4(billboard * bottomRight + pos.xyz, 1.0f);
    gTexCoord = vec2(1.0f, 1.0f);
    EmitVertex();
}