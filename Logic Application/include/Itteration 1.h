#pragma once

#include "Application.h"

class Model;
class ShaderProgram;

namespace Itter1
{
	class ApplicationG : public Application
	{
	private:
		Model* mp_model = nullptr;
		ShaderProgram* mp_shaderProgram = nullptr;
	protected:

	public:
		ApplicationG();
		~ApplicationG();

		void Update();
		void ShadowDraw();
		void Draw();
		void GUIDraw();
	};
}