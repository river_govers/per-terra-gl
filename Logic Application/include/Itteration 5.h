#include "Application.h"

#include "Rendering\Shaders.h"

#include "Rendering\Lights.h"

#include "Rendering\Skinning\Texture.h"
#include "Rendering\Skinning\Skybox.h"
#include "Rendering\Models\Terrain.h"
#include "Rendering\Models\Model.h"

namespace Itter5
{
	class ApplicationG : public Application
	{
	private:
		Terrain* mp_terrain;
		ShaderProgram* mp_terrainShaderProgram;
		Texture* mp_texture = nullptr;

		Model* mp_selectionSphere = nullptr;
		ShaderProgram* mp_selectionShaderProgram;

		Font* mp_font;
		ShaderProgram* mp_fontShader;
		
		ShaderProgram* mp_shadowCasterShaderProgram;

		DirectionalLight* m_directionalLight;
		ShaderProgram* mp_directionalLightShader;

		Skybox* mp_skybox;
		ShaderProgram* mp_skyboxShader;

		void Generate();

		unsigned short m_x = 0, m_y = 0, m_z = 0;

		float xDelta = 0.0f, yDelta = 0.0f, zDelta = 0.0f;

		bool m_menuDisplay = false;
		int m_selection = 0;

		bool m_startPressed = false;
		bool m_leftDPadPressed = false;
		bool m_rightDPadPressed = false;
		bool m_upDPadPressed = false;
		bool m_downDPadPressed = false;

		int m_brushSize = 1;

		void DrawMenuItem(const char* a_string, int a_index, const glm::mat4& a_projection);
	protected:

	public:
		ApplicationG();
		~ApplicationG();

		void Update();

		void ShadowDraw();
		void Draw();
		void GUIDraw();
	};
}