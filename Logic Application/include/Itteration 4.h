#pragma once

#include "Application.h"

#include "Rendering\Shaders.h"

#include "Rendering\Models\Model.h"

namespace Itter4
{
	class ApplicationG : public Application
	{
	private:
		Model** mp_model;
		int m_models;

		ShaderProgram* mp_shaderProgram;
	protected:

	public:
		ApplicationG();
		~ApplicationG();

		void Update();

		void ShadowDraw();
		void Draw();
		void GUIDraw();
	};
}