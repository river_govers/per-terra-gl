#pragma once

#include "Application.h"

#include "Rendering\Particle System.h"

#include "Rendering\Skinning\Texture.h"

namespace Itter3
{
	class ApplicationG : public Application
	{
	private:
		ParticleEmitter* mp_particleEmitter;
		ShaderProgram* mp_particleShader;
		Texture* mp_particleTexture = nullptr;
	protected:

	public:
		ApplicationG();
		~ApplicationG();

		void Update();
		void ShadowDraw();
		void Draw();
		void GUIDraw();
	};
}