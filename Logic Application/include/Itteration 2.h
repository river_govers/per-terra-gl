#pragma once

#include "Application.h"

#include "Rendering/Shaders.h"
#include "Rendering/Models/Model.h"
#include "Rendering/Models/Terrain.h"

#include "Maths/Transform.h"

class DirectionalLight;

namespace Itter2
{
	class ApplicationG : public Application
	{
	private:
		int m_modelsSize;
		Model** mp_model = nullptr;

		Texture* mp_texture = nullptr;
		Texture* mp_normalMap = nullptr;
		Texture* mp_specularMap = nullptr;
		ShaderProgram* mp_shaderCaster = nullptr;
		ShaderProgram* mp_shader = nullptr;

		Transform m_transform;

		Model* mp_cameraQuad = nullptr;
		ShaderProgram* mp_cameraShaderProgram = nullptr;
		RenderTarget* mp_cameraOutput = nullptr;

		DirectionalLight* m_light;
	protected:

	public:
		ApplicationG();
		~ApplicationG();

		void Update();
		void ShadowDraw();
		void Draw();
		void GUIDraw();
	};
}