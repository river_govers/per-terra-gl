#include "Itteration 5.h"

#include "Rendering\Pipeline.h"
#include "Rendering\GraphicUI.h"
#include "Rendering\Camera.h"

#include "Rendering\Skinning\Font.h"

#include "Maths\Perlin2D.h"

#include "Input.h"
#include "TimeG.h"

#include <algorithm>

namespace Itter5
{
	const char* menuItems[] = { "X: ", "Y: ", "Z: ", "Brush Size: ", "Create", "Destroy", "Regenerate" };

	void ApplicationG::Generate()
	{
		PerlinNoise2D noiseGen = PerlinNoise2D((int)time(NULL), 15.0f, 0.025f);

		for (int x = 0; x < 32; ++x)
		{
			for (int y = 0; y < 32; ++y)
			{
				// Generate noise value
				float height = (float)noiseGen.OctaveNoise(x, y, 3, 0.5f);

				for (int i = 0; i < 32; ++i)
				{
					// Set all the tiles up to the noise value to true
					mp_terrain->TerrainData(x, i, y).state = i < (int)height;
					//mp_terrain->TerrainData(x, i, y).state = i < 1;
				}
			}
		}

		mp_terrain->UpdateMesh(1.0f);
	}
	void ApplicationG::DrawMenuItem(const char* a_string, int a_index, const glm::mat4& a_projection)
	{
		// If the current selection is this draw the highlighted
		if (m_selection == a_index)
		{
			GraphicUI::DrawString(a_string, { 15.0f, -10.0f + a_index * -32 }, { 1.0f, 1.0f }, 32, { 1.0f, 0.0f, 0.0f, 1.0f }, *mp_font, *mp_fontShader, a_projection);

			return;
		}

		GraphicUI::DrawString(a_string, { 10.0f, -10.0f + a_index * -32 }, { 1.0f, 1.0f }, 28, { 1.0f, 1.0f, 1.0f, 1.0f }, *mp_font, *mp_fontShader, a_projection);
	}
	ApplicationG::ApplicationG() : Application(1280, 720)
	{
		new Camera(glm::pi<float>() * 0.45f, 0.1f, 1000.0f, GetWindowWidth() / (float)GetWindowHeight());

		m_directionalLight = new DirectionalLight();

		m_directionalLight->SetLightColor({ 0.55f, 0.55f, 0.5f, 1.0f });
		m_directionalLight->SetLightDirection(glm::normalize(glm::vec3(0.0f, -1.0f, 0.0f)));

		mp_terrain = new Terrain(32, 0.25f);
		mp_terrainShaderProgram = new ShaderProgram({ PixelShader::LoadShader("./Shaders/terrain.frag").c_str() }, { GeometryShader::LoadShader("./Shaders/terrain.geom").c_str() }, { VertexShader::LoadShader("./Shaders/terrain.vert").c_str() });
		mp_shadowCasterShaderProgram = new ShaderProgram({ PixelShader::LoadShader("./Shaders/standardSCaster.frag").c_str() }, { VertexShader::LoadShader("./Shaders/standardSCaster.vert").c_str() });

		Generate();

		mp_selectionShaderProgram = new ShaderProgram({ PixelShader::LoadShader("./Shaders/standard.frag").c_str() }, { VertexShader::LoadShader("./Shaders/standard.vert").c_str() });

		Model::GenerateIcoSphere(mp_selectionSphere, 2);

		Texture::LoadFile("./Textures/earth_diffuse.jpg", mp_texture);

		mp_terrainShaderProgram->SetTexture("diffuse", mp_texture);

		mp_font = new Font("./Fonts/Cousine-Regular.ttf");
		mp_fontShader = new ShaderProgram({ PixelShader::LoadShader("./Shaders/GUI/font.frag").c_str() }, { VertexShader::LoadShader("./Shaders/GUI/font.vert").c_str() });

		const const char* const fileNames[6] = 
		{ 
			"./Textures/Skybox/spires_ft.tga", 
			"./Textures/Skybox/spires_bk.tga",
			"./Textures/Skybox/spires_up.tga", 
			"./Textures/Skybox/spires_dn.tga", 
			"./Textures/Skybox/spires_rt.tga",
			"./Textures/Skybox/spires_lf.tga", 
		};

		mp_skybox = new Skybox(fileNames);
		mp_skyboxShader = new ShaderProgram({ PixelShader::LoadShader("./Shaders/skybox.frag").c_str() }, { VertexShader::LoadShader("./Shaders/skybox.vert").c_str() });

		mp_directionalLightShader = new ShaderProgram({ PixelShader::LoadShader("./Shaders/directionalLight.frag").c_str() }, { VertexShader::LoadShader("./Shaders/directionalLight.vert").c_str() });

		m_x = 16;
		m_y = 16;
		m_z = 16;
	}
	ApplicationG::~ApplicationG()
	{
		delete mp_terrain;
		delete mp_terrainShaderProgram;
		delete mp_shadowCasterShaderProgram;
		
		delete mp_font;
		delete mp_fontShader;

		delete mp_texture;

		delete mp_selectionSphere;
		delete mp_selectionShaderProgram;

		delete mp_skyboxShader;
		delete mp_skybox;

		delete mp_directionalLightShader;
	}

	bool GetPress(int a_controller, e_GamepadButton a_button, bool& a_state)
	{
		if (InputManager::GetGamepadButtonDown(0, a_button) && !a_state)
		{
			a_state = true;

			return true;
		}
		else if (!InputManager::GetGamepadButtonDown(0, a_button))
		{
			a_state = false;
		}

		return false;
	}

	void ApplicationG::Update()
	{
		// Menu display toggle
		if (GetPress(0, e_GamepadButton::Start, m_startPressed))
		{
			m_menuDisplay = !m_menuDisplay;
		}

		if (!m_menuDisplay)
		{
			float lX = InputManager::GetGamepadAxis(0, e_Axis::LeftX);
			float lY = InputManager::GetGamepadAxis(0, e_Axis::LeftY);
			float lZ = InputManager::GetGamepadAxis(0, e_Axis::RightTrigger) - InputManager::GetGamepadAxis(0, e_Axis::LeftTrigger);

			float rX = InputManager::GetGamepadAxis(0, e_Axis::RightX);
			float rY = InputManager::GetGamepadAxis(0, e_Axis::RightY);
			float rZ = InputManager::GetGamepadButtonDown(0, e_GamepadButton::RightBumper) - (float)InputManager::GetGamepadButtonDown(0, e_GamepadButton::LeftBumper);

			Transform& transform = Camera::GetPrimaryCamera()->Transform();

			transform.Translate(transform.Forward() * -lY * (float)TimeG::DeltaTime() * 10.0f);
			transform.Translate(transform.Right() * lX * (float)TimeG::DeltaTime() * 10.0f);
			transform.Translate(transform.Up() * lZ * (float)TimeG::DeltaTime() * 10.0f);

			transform.Rotate({ -rY * (float)TimeG::DeltaTime() * 5.0f, rX * (float)TimeG::DeltaTime() * 5.0f, rZ * (float)TimeG::DeltaTime() * 5.0f });
		}
		else
		{
			// Menu movement handling
			if (GetPress(0, e_GamepadButton::DPadLeft, m_leftDPadPressed))
			{
				switch (m_selection)
				{
				case 0:
				{
					if (--m_x > 31)
					{
						m_x = 31;
					}

					break;
				}
				case 1:
				{
					if (--m_y > 31)
					{
						m_y = 31;
					}

					break;
				}
				case 2:
				{
					if (--m_z > 31)
					{
						m_z = 31;
					}

					break;
				}
				case 3:
				{
					if (--m_brushSize < 0)
					{
						m_brushSize = 0;
					}

					break;
				}
				}
			}			
			if (GetPress(0, e_GamepadButton::DPadRight, m_rightDPadPressed))
			{
				switch (m_selection)
				{
				case 0:
				{
					if (++m_x > 31)
					{
						m_x = 0;
					}

					break;
				}
				case 1:
				{
					if (++m_y > 31)
					{
						m_y = 0;
					}

					break;
				}
				case 2:
				{
					if (++m_z > 31)
					{
						m_z = 0;
					}

					break;
				}
				case 3:
				{
					++m_brushSize;

					break;
				}
				}
			}
			if (GetPress(0, e_GamepadButton::DPadUp, m_upDPadPressed))
			{
				if (--m_selection < 0)
				{
					m_selection = ARRAYSIZE(menuItems) - 1;
				}
			}
			if (GetPress(0, e_GamepadButton::DPadDown, m_downDPadPressed))
			{
				if (++m_selection >= ARRAYSIZE(menuItems))
				{
					m_selection = 0;
				}
			}

			// Menu input handling
			if (InputManager::GetGamepadButtonDown((e_GamePadNum)0b11111111, e_GamepadButton::ButtonA))
			{
				switch (m_selection)
				{
				case 4:
				{
					for (int x = -m_brushSize; x < m_brushSize; ++x)
					{
						for (int y = -m_brushSize; y < m_brushSize; ++y)
						{
							for (int z = -m_brushSize; z < m_brushSize; ++z)
							{
								if ((float)m_brushSize > glm::length(glm::vec3((float)x, (float)y, (float)z)))
								{
#ifdef _DEBUG
									int xIndex = std::max(0, std::min((int)m_x + x, 31));
									int yIndex = std::max(0, std::min((int)m_y + y, 31));
									int zIndex = std::max(0, std::min((int)m_z + z, 31));
#else
									int xIndex = max(0, min((int)m_x + x, 31));
									int yIndex = max(0, min((int)m_y + y, 31));
									int zIndex = max(0, min((int)m_z + z, 31));
#endif

									mp_terrain->TerrainData((unsigned short)xIndex, (unsigned short)yIndex, (unsigned short)zIndex).state = true;
								}
							}
						}
					}

					mp_terrain->UpdateMesh(1.0f);

					break;
				}
				case 5:
				{
					for (int x = -m_brushSize; x < m_brushSize; ++x)
					{
						for (int y = -m_brushSize; y < m_brushSize; ++y)
						{
							for (int z = -m_brushSize; z < m_brushSize; ++z)
							{
								if ((float)m_brushSize > glm::fastLength(glm::vec3((float)x, (float)y, (float)z)))
								{
#ifdef _DEBUG
									int xIndex = std::max(0, std::min((int)m_x + x, 31));
									int yIndex = std::max(0, std::min((int)m_y + y, 31));
									int zIndex = std::max(0, std::min((int)m_z + z, 31));
#else
									int xIndex = max(0, min((int)m_x + x, 31));
									int yIndex = max(0, min((int)m_y + y, 31));
									int zIndex = max(0, min((int)m_z + z, 31));
#endif // _DEBUG

									mp_terrain->TerrainData((unsigned short)xIndex, (unsigned short)yIndex, (unsigned short)zIndex).state = false;
								}
							}
						}
					}

					mp_terrain->UpdateMesh(1.0f);

					break;
				}
				case 6:
				{
					Generate();

					break;
				}
				}
			}
		}

		if (InputManager::GetGamepadButtonDown(0, e_GamepadButton::Back))
		{
			delete mp_terrainShaderProgram;

			mp_terrainShaderProgram = new ShaderProgram({ PixelShader::LoadShader("./Shaders/terrain.frag").c_str() }, { GeometryShader::LoadShader("./Shaders/terrain.geom").c_str() }, { VertexShader::LoadShader("./Shaders/terrain.vert").c_str() });
			mp_terrainShaderProgram->SetTexture("diffuse", mp_texture);
		}

		static glm::vec4 lightDir = { 0.0f, 1.0f, 0.0f, 1.0f };
		glm::mat4 rotMatrix = glm::rotate(glm::pi<float>() * 0.25f * (float)TimeG::DeltaTime(), glm::vec3(1.0f, 0.0f, 0.0f));
		lightDir = rotMatrix * lightDir;
		m_directionalLight->SetLightDirection(glm::vec3(lightDir));
	}

	void ApplicationG::ShadowDraw()
	{
		Pipeline::DrawTerrain(glm::mat4(1.0f), *mp_shadowCasterShaderProgram, *mp_terrain);
	}
	void ApplicationG::Draw()
	{
		Pipeline::DrawSkybox(*mp_skyboxShader, *mp_skybox);
		//Pipeline::DrawDirectionalLight(*mp_directionalLightShader, *m_directionalLight);

		if (m_menuDisplay)
		{
			Pipeline::DrawModelCulled(glm::translate(glm::vec3(m_x * 0.25f, m_y * 0.25f, m_z * 0.25f)) * glm::scale(glm::vec3(m_brushSize * 0.25f, m_brushSize * 0.25f, m_brushSize * 0.25f)), *mp_selectionShaderProgram, *mp_selectionSphere);
		}

		if (InputManager::GetGamepadButtonDown(e_GamePadNum::All, e_GamepadButton::Back))
		{
			unsigned short res = mp_terrain->GetResolution();
			float scaling = mp_terrain->GetSpacing();

			for (unsigned short x = 0; x < res; ++x)
			{
				for (unsigned short y = 0; y < res; ++y)
				{
					for (unsigned short z = 0; z < res; ++z)
					{
						if (mp_terrain->TerrainData(x, y, z).state)
						{
							Pipeline::DrawModelCulled(glm::translate(glm::vec3(x * scaling, y * scaling, z * scaling)) * glm::scale(glm::vec3(0.025f)), *mp_selectionShaderProgram, *mp_selectionSphere);
						}
					}
				}
			}
		}
		else
		{
			Pipeline::DrawTerrain(glm::mat4(1.0f), *mp_terrainShaderProgram, *mp_terrain);
		}
	}
	void ApplicationG::GUIDraw()
	{
		if (m_menuDisplay)
		{
			// Centers text on top left
			const glm::mat4 projection = { 2.0f / GetWindowWidth(), 0.0f,					  0.0f,  0.0f,
										   0.0f,					2.0f / GetWindowHeight(), 0.0f,  0.0f,
										   0.0f,					0.0f,					  -1.0f, 0.0f,
										   -1.0f,					1.0f,					  0.0f,  1.0f };

			for (int i = 0; i < ARRAYSIZE(menuItems); ++i)
			{
				// Menu drawing
				switch (i)
				{
				case 0:
				{
					std::string string = menuItems[i] + std::to_string(m_x);

					DrawMenuItem(string.c_str(), i, projection);

					break;
				}
				case 1:
				{
					std::string string = menuItems[i] + std::to_string(m_y);

					DrawMenuItem(string.c_str(), i, projection);

					break;
				}
				case 2:
				{
					std::string string = menuItems[i] + std::to_string(m_z);

					DrawMenuItem(string.c_str(), i, projection);

					break;

				}
				case 3:
				{
					std::string string = menuItems[i] + std::to_string(m_brushSize);

					DrawMenuItem(string.c_str(), i, projection);

					break;
				}
				default:
				{
					DrawMenuItem(menuItems[i], i, projection);

					break;
				}
				}
			}
		}
	}
}