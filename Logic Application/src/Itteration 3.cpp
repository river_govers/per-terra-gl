#include "Itteration 3.h"

#include "Rendering\Camera.h"
#include "Rendering\Shaders.h"
#include "Rendering\Pipeline.h"

#include "TimeG.h"
#include "Input.h"

namespace Itter3
{
	ApplicationG::ApplicationG() : Application(1280, 720)
	{
		new Camera(glm::pi<float>() * 0.45f, 0.1f, 1000.0f, GetWindowWidth() / (float)GetWindowHeight());

		PixelShader particlePixelShader = { PixelShader::LoadShader("./Shaders/particle.frag").c_str() };
		GeometryShader particleGeomShader = { GeometryShader::LoadShader("./Shaders/particle.geom").c_str() };
		VertexShader particleVertShader = { VertexShader::LoadShader("./Shaders/particle.vert").c_str() };

		mp_particleShader = new ShaderProgram(particlePixelShader, particleGeomShader, particleVertShader);
		
		mp_particleEmitter = new ParticleEmitter(1000, 100, 0.25f, 10.0f, 0.25f, 2.0f, { 0.25f, 2.5f , { 1.0f, 0.0f, 0.0f, 1.0f }, { 0.0f, 1.0f, 0.0f, 1.0f }, nullptr }, *mp_particleShader);

		Texture::LoadFile("./Textures/circle.png", mp_particleTexture);

		mp_particleShader->SetTexture("diffuse", mp_particleTexture);
	}
	ApplicationG::~ApplicationG()
	{
		delete mp_particleShader;
		delete mp_particleEmitter;
		delete mp_particleTexture;
	}

	void ApplicationG::Update()
	{
		Camera::GetPrimaryCamera();

		float lX = InputManager::GetGamepadAxis(0, e_Axis::LeftX);
		float lY = InputManager::GetGamepadAxis(0, e_Axis::LeftY);
		float lZ = InputManager::GetGamepadAxis(0, e_Axis::RightTrigger) - InputManager::GetGamepadAxis(0, e_Axis::LeftTrigger);

		float rX = InputManager::GetGamepadAxis(0, e_Axis::RightX);
		float rY = InputManager::GetGamepadAxis(0, e_Axis::RightY);
		float rZ = InputManager::GetGamepadButtonDown(0, e_GamepadButton::RightBumper) - (float)InputManager::GetGamepadButtonDown(0, e_GamepadButton::LeftBumper);

		Transform& transform = Camera::GetPrimaryCamera()->Transform();

		transform.Translate(transform.Forward() * -lY * (float)TimeG::DeltaTime() * 10.0f);
		transform.Translate(transform.Right() * lX * (float)TimeG::DeltaTime() * 10.0f);
		transform.Translate(transform.Up() * lZ * (float)TimeG::DeltaTime() * 10.0f);

		transform.Rotate({ -rY * (float)TimeG::DeltaTime() * 5.0f, rX * (float)TimeG::DeltaTime() * 5.0f, rZ * (float)TimeG::DeltaTime() * 5.0f });

		if (InputManager::GetGamepadButtonDown(0, e_GamepadButton::ButtonA))
		{
			delete mp_particleShader;

			std::string pShader = PixelShader::LoadShader("./particle.frag");
			std::string gShader = GeometryShader::LoadShader("./particle.geom");
			std::string vShader = VertexShader::LoadShader("./particle.vert");

			mp_particleShader = new ShaderProgram({ pShader.c_str() }, { gShader.c_str() }, { vShader.c_str() });
			mp_particleShader->SetTexture("diffuse", mp_particleTexture);

			mp_particleEmitter->SetShader(*mp_particleShader);
		}

		mp_particleEmitter->Update();
	}
	void ApplicationG::ShadowDraw()
	{

	}
	void ApplicationG::Draw()
	{
		Pipeline::DrawParticleSystem(*mp_particleEmitter);
	}
	void ApplicationG::GUIDraw()
	{

	}
}
