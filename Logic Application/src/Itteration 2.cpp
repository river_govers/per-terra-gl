#include "Itteration 2.h"

#include "Rendering/Pipeline.h"
#include "Rendering/Camera.h"
#include "Rendering/Lights.h"

#include "Input.h"
#include "TimeG.h"

#include "Maths/Perlin2D.h"

#include "gl_core_4_4.h"

namespace Itter2
{
	ApplicationG::ApplicationG() : Application(1280, 720)
	{
		Texture::LoadFile("./Textures/soulspear_diffuse.tga", mp_texture);
		Texture::LoadFile("./Textures/soulspear_normal.tga", mp_normalMap);
		Texture::LoadFile("./Textures/soulspear_specular.tga", mp_specularMap);

		std::string vShaderS = VertexShader::LoadShader("./Shaders/standardLit.vert");
		std::string pShaderS = PixelShader::LoadShader("./Shaders/standardLit.frag");
		std::string vShaderCasterS = VertexShader::LoadShader("./Shaders/standardSCaster.vert");
		std::string pShaderCasterS = PixelShader::LoadShader("./Shaders/standardSCaster.frag");
		std::string vShaderU = VertexShader::LoadShader("./Shaders/standard.vert");
		std::string pShaderU = PixelShader::LoadShader("./Shaders/standard.frag");

		Model::LoadOBJ("./Models/soulspear.obj", mp_model, m_modelsSize);

		mp_shaderCaster = new ShaderProgram({ pShaderCasterS.c_str() }, { vShaderCasterS.c_str() });

		mp_shader = new ShaderProgram({ pShaderS.c_str() }, { vShaderS.c_str() });
		mp_shader->SetTexture("diffuse", mp_texture);
		mp_shader->SetTexture("normalMap", mp_normalMap);
		mp_shader->SetTexture("specularMap", mp_specularMap);

		m_light = new DirectionalLight();

		m_light->SetLightColor({ 0.75f, 0.25f, 0.75f, 1.0f });

		Model::GenerateQuad(mp_cameraQuad);

		mp_cameraShaderProgram = new ShaderProgram({ pShaderU.c_str() }, { vShaderU.c_str() });

		new Camera(glm::pi<float>() * 0.45f, 0.1f, 1000.0f, GetWindowWidth() / (float)GetWindowHeight());

		mp_cameraOutput = new RenderTarget({ 0, 0, 2048, 2048 }, GL_RGBA, GL_RGBA, GL_COLOR_ATTACHMENT0);
		Camera* camera = new Camera(glm::pi<float>() * 0.45f, 0.1f, 1000.0f, (float)GetWindowWidth() / GetWindowHeight(), GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		camera->SetRenderTarget(mp_cameraOutput);

		mp_cameraShaderProgram->SetTexture("diffuse", &mp_cameraOutput->GetTexture());
	}

	ApplicationG::~ApplicationG()
	{
		delete mp_texture;
		delete mp_normalMap;
		delete mp_specularMap;
		delete mp_shader;
		for (int i = 0; i < m_modelsSize; ++i)
		{
			delete mp_model[i];
		}
		delete mp_model;
		delete mp_shaderCaster;

		delete mp_cameraQuad;
		delete mp_cameraShaderProgram;
		delete mp_cameraOutput;
	}

	void ApplicationG::Update()
	{
		float lX = InputManager::GetGamepadAxis(0, e_Axis::LeftX);
		float lY = InputManager::GetGamepadAxis(0, e_Axis::LeftY);
		float lZ = InputManager::GetGamepadAxis(0, e_Axis::RightTrigger) - InputManager::GetGamepadAxis(0, e_Axis::LeftTrigger);

		float rX = InputManager::GetGamepadAxis(0, e_Axis::RightX);
		float rY = InputManager::GetGamepadAxis(0, e_Axis::RightY);
		float rZ = InputManager::GetGamepadButtonDown(0, e_GamepadButton::RightBumper) - (float)InputManager::GetGamepadButtonDown(0, e_GamepadButton::LeftBumper);

		Transform& transform = Camera::GetPrimaryCamera()->Transform();

		transform.Translate(transform.Forward() * -lY * (float)TimeG::DeltaTime() * 10.0f);
		transform.Translate(transform.Right() * lX * (float)TimeG::DeltaTime() * 10.0f);
		transform.Translate(transform.Up() * lZ * (float)TimeG::DeltaTime() * 10.0f);

		transform.Rotate({ -rY * (float)TimeG::DeltaTime() * 5.0f, rX * (float)TimeG::DeltaTime() * 5.0f, rZ * (float)TimeG::DeltaTime() * 5.0f });

		if (InputManager::GetGamepadButtonDown(0, e_GamepadButton::ButtonA))
		{
			delete mp_shader;

			std::string vShaderS = VertexShader::LoadShader("./Shaders/standardLit.vert");
			std::string pShaderS = PixelShader::LoadShader("./Shaders/standardLit.frag");

			mp_shader = new ShaderProgram({ pShaderS.c_str() }, { vShaderS.c_str() });
			mp_shader->SetTexture("diffuse", mp_texture);
			mp_shader->SetTexture("normalMap", mp_normalMap);
			mp_shader->SetTexture("specularMap", mp_specularMap);
		}

		static glm::vec4 lightDir(1, 0, 0, 0);
		glm::mat4 rotMatrix = glm::rotate(glm::pi<float>() * 0.25f * (float)TimeG::DeltaTime(), glm::vec3(0, 1, 0));
		lightDir = rotMatrix * lightDir;
		m_light->SetLightDirection(glm::vec3(lightDir.x, lightDir.y, lightDir.z));
	}

	void ApplicationG::ShadowDraw()
	{
		for (int i = 0; i < m_modelsSize; ++i)
		{
			Pipeline::DrawModel(m_transform.GetMatrix(), *mp_shaderCaster, *mp_model[i]);
		}

		/*Pipeline::DrawModel(glm::translate(glm::vec3(5.0f, 0.0f, 0.0f)), *mp_shaderCaster, *mp_cameraQuad);*/
	}

	void ApplicationG::Draw()
	{
		for (int i = 0; i < m_modelsSize; ++i)
		{
			Pipeline::DrawModel(m_transform.GetMatrix(), *mp_shader, *mp_model[i]);
		}

		Pipeline::DrawModel(glm::translate(glm::vec3(2.5f, 0.0f, 0.0f)), *mp_cameraShaderProgram, *mp_cameraQuad);
	}

	void ApplicationG::GUIDraw()
	{

	}
}