#include "Itteration 1.h"

#include "glm\glm.hpp"

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#define DEBGUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBGUG_NEW
#endif 

#include "Rendering/Camera.h"
#include "Rendering/Models/Model.h"
#include "Rendering/Shaders.h"
#include "Rendering/Pipeline.h"

#include "Input.h"

#include "TimeG.h"

const bool gamePads[4] = { true, true, true, true };

namespace Itter1
{
	#if _OPEN_GL
	ApplicationG::ApplicationG() : Application(1280, 720)
	{
		new Camera(glm::pi<float>() * 0.45f, 0.1f, 1000.0f, GetWindowWidth() / (float)GetWindowHeight());

		Model::GenerateQuad(mp_model);

		mp_shaderProgram = new ShaderProgram({ PixelShader::LoadShader("./Shaders/standard.frag").c_str() }, { VertexShader::LoadShader("./Shaders/standard.vert").c_str() } );
	}
	#endif

	ApplicationG::~ApplicationG()
	{
		delete mp_model;
		delete mp_shaderProgram;
	}

	void ApplicationG::Update()
	{
		float x = InputManager::GetGamepadAxis(0, e_Axis::LeftX);
		float y = InputManager::GetGamepadAxis(0, e_Axis::LeftY);
		Transform& camTran = Camera::GetPrimaryCamera()->Transform();

		camTran.Translate(camTran.Forward() * y * (float)TimeG::DeltaTime() * -10.0f);
		camTran.Translate(camTran.Right() * x * (float)TimeG::DeltaTime() * 10.0f);
	}

	void ApplicationG::ShadowDraw()
	{

	}
	void ApplicationG::Draw()
	{
		Pipeline::DrawModel(glm::mat4(1), *mp_shaderProgram, *mp_model);
	}
	void ApplicationG::GUIDraw()
	{

	}
}
