#include "..\include\Itteration 4.h"

#include "Rendering\Camera.h"
#include "Rendering\Pipeline.h"

#include "Input.h"
#include "TimeG.h"

namespace Itter4
{
	ApplicationG::ApplicationG() : Application(1280, 720)
	{
		new Camera(glm::pi<float>() * 0.45f, 0.1f, 1000.0f, GetWindowWidth() / (float)GetWindowHeight());

		Model::LoadOBJ("./Models/soulspear.obj", mp_model, m_models);

		mp_shaderProgram = new ShaderProgram({ PixelShader::LoadShader("./Shaders/standard.frag").c_str() }, { VertexShader::LoadShader("./Shaders/standard.vert").c_str() });
	}
	ApplicationG::~ApplicationG()
	{
		for (int i = 0; i < m_models; ++i)
		{
			delete mp_model[i];
		}

		delete[] mp_model;

		delete mp_shaderProgram;
	}

	void ApplicationG::Update()
	{
		float rX = InputManager::GetGamepadAxis(0, e_Axis::RightX);
		float rY = InputManager::GetGamepadAxis(0, e_Axis::RightY);
		float rZ = InputManager::GetGamepadButtonDown(0, e_GamepadButton::RightBumper) - (float)InputManager::GetGamepadButtonDown(0, e_GamepadButton::LeftBumper);

		Transform& transform = Camera::GetPrimaryCamera()->Transform();

		transform.Rotate({ -rY * (float)TimeG::DeltaTime() * 5.0f, rX * (float)TimeG::DeltaTime() * 5.0f, rZ * (float)TimeG::DeltaTime() * 5.0f });
	}

	void ApplicationG::ShadowDraw()
	{
	}
	void ApplicationG::Draw()
	{
		for (int i = 0; i < m_models; ++i)
		{
			Pipeline::DrawModelCulled(glm::mat4(1.0f), *mp_shaderProgram, *mp_model[i]);
		}
	}
	void ApplicationG::GUIDraw()
	{

	}
}