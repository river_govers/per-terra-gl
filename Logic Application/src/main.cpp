#include "Itteration 1.h"
#include "Itteration 2.h"
#include "Itteration 3.h"
#include "Itteration 4.h"
#include "Itteration 5.h"

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#define DEBGUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBGUG_NEW
#endif 

// My convention with points are weird but they work
// They allow me to keep track of which class to delete them in without having to keep a huge diagram
// They also allow me to write out the deconstructor without looking at the header because when intellisense works it give me a list of all the pointers
// It also makes tracking down memory leaks easier and faster for me
// Quit bugging me about it people

// If it fails to build or insists about memory corruption clean the solution and rebuild then take it from there

#if _OPEN_GL
void main()
{
	// Use this to switch apps
	using namespace Itter5;

#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	Application* app = new ApplicationG();
	app->Run();
	delete app;
}
#endif

// Never got around to fixing
#if _DIRECT_X
#include <Windows.h>

int WINAPI WinMain(HINSTANCE a_hinstance, HINSTANCE a_hPrevInstance, LPSTR a_lpCmdLine, int a_nCmdShow)
{
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	Application* app = new ApplicationG(a_hinstance, a_nCmdShow);
	app->Run();
	delete app;

	return 0;
}
#endif