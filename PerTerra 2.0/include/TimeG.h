#pragma once

class TimeG
{
private:
	friend class Application;

	static TimeG* m_time;

	double m_deltaTime;
	double m_timeScale;
	double m_timePassed;
protected:

public:
	static double DeltaTime();
	static double UnscaledDeltaTime();
	static double TimePassed();

	static void SetTimeScale(double a_timeScale);
};