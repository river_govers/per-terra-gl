#pragma once

#if _WIN32
#include <Windows.h>
#include <XInput.h>

#define MAX_GAMEPADS XUSER_MAX_COUNT
#define XINPUT
#define GAMEPAD_THUMBCAP 32767
#define GAMEPAD_TRIGGERCAP 255;
#elif _OPEN_GL
#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>

#define MAX_GAMEPADS GLFW_JOYSTICK_LAST
#endif

enum class e_GamePadNum : unsigned char
{
	None = 0b00000000,
	One =	0b00000001,
	Two =	0b00000010,
	Three = 0b00000100,
	Four =	0b00001000,
	Five =  0b00010000,
	Six =	0b00100000,
	Seven = 0b01000000,
	Eight = 0b10000000,
	All = 0b11111111,
	First = None,
	Last = All
};

enum class e_InputDevice
{
	Keyboard,
	Mouse,
	Gamepad
};
enum class e_Axis
{
	LeftX,
	LeftY,
	RightX,
	RightY,
	LeftTrigger,
	RightTrigger
};
enum class e_GamepadButton
{
	DPadUp				 = 0x0001,
	DPadDown			 = 0x0002,
	DPadLeft			 = 0x0004,
	DPadRight			 = 0x0008,
	Start				 = 0x0010,
	Back				 = 0x0020,
	LeftThumbstickClick  = 0x0040,
	RightThumbstickClick = 0x0080,
	LeftBumper			 = 0x0100,
	RightBumper			 = 0x0200,
	ButtonA				 = 0x1000,
	ButtonB				 = 0x2000,
	ButtonX				 = 0x4000,
	ButtonY				 = 0x8000
};						   
						   
struct Controller
{
	bool state;

#ifdef XINPUT
	XINPUT_STATE gamepadState;
#else
	const char* name;
#endif
};

class InputManager
{
private:
	friend class Application;

	static InputManager* mp_inputManager;

	Controller m_gamePads[MAX_GAMEPADS];
#ifdef XINPUT
	unsigned short m_leftDeadzone;
	unsigned short m_rightDeadzone;
	unsigned short m_triggerDeadzone;
#endif // XINPUT
protected:

public:
	InputManager();
	~InputManager();

	static void Update();

	static float GetGamepadAxis(int a_gamePad, e_Axis a_axis);
	static bool GetGamepadButtonDown(e_GamePadNum a_gamePad, e_GamepadButton a_button);
	static bool GetGamepadButtonDown(int a_gamePad, e_GamepadButton a_button);

#ifdef XINPUT
	static void SetLeftDeadzone(unsigned short a_mag);
	static void SetRightDeadzone(unsigned short a_mag);
	static void SetTriggerDeadzone(unsigned short a_mag);
#endif
};