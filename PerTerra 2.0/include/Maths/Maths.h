#pragma once

#if _DIRECT_X
#include <DirectXMath.h>
#endif

#if _OPEN_GL
#define GLM_SWIZZLE
#include "glm\glm.hpp"
#include "glm\ext.hpp"
#include "glm\gtx\matrix_decompose.hpp"
#endif

// TODO: Rewrite maths class

int Power2Roundup(int a_x);
float InvSqrt(float a_num);

class Vector3
{
private:
	friend class Matrix4;

#if _DIRECT_X
	DirectX::XMVECTOR m_dxVector;
#endif // 

#if _OPEN_GL
	glm::vec3 m_glVector;
#endif

protected:

public:
	Vector3();
#if _OPEN_GL
	Vector3(const glm::vec3& a_vector);
#endif
#if _DIRECT_X
	Vector3(const DirectX::XMVECTOR& a_vector);
#endif
	Vector3(float a_x, float a_y, float a_z);

	float GetX() const;
	float GetY() const;
	float GetZ() const;
	float GetIndex(int a_index) const;

	void SetX(float a_x);
	void SetY(float a_y);
	void SetZ(float a_z);
	void SetIndex(int a_index, float a_value);
	
	Vector3 operator =(const Vector3& a_vector);

	Vector3 operator *(float a_scalar) const;
	Vector3 operator /(float a_scalar) const;
	Vector3 operator +(const Vector3& a_vector) const;
	Vector3 operator -(const Vector3& a_vector) const;

	Vector3 operator *=(float a_scalar);
	Vector3 operator /=(float a_scalar);
	Vector3 operator +=(const Vector3& a_vector);
	Vector3 operator -=(const Vector3& a_vector);

	Vector3 Normalized() const;
	void Normalize();

	static Vector3 Cross(const Vector3& a_vectorA, const Vector3& a_vectorB);
};

class Matrix4
{
private:
	friend class Pipeline;

	union 
	{
		float m_matix[16];

		float m_mat4x4[4][4];
	};

protected:

public:
	Matrix4();
#if _OPEN_GL
	Matrix4(const glm::mat4& a_matrix);
	explicit operator glm::mat4() const;
#endif
#if _DIRECT_X
	Matrix4(const DirectX::XMMATRIX& a_matrix);
#endif
	Matrix4(float a_00, float a_01, float a_02, float a_03,
			float a_10, float a_11, float a_12, float a_13,
			float a_20, float a_21, float a_22, float a_23,
			float a_30, float a_31, float a_32, float a_33);

	float GetIndex(int a_index) const;
	float GetIndex(int a_x, int a_y) const;

	void SetIndex(int a_index, float a_value);
	void SetIndex(int a_x, int a_y, float a_value);

	Matrix4& operator =(const Matrix4& a_matrix);

	Matrix4 operator *(const Matrix4& a_matrix) const;
	Matrix4 operator +(const Matrix4& a_matrix) const;
	Matrix4 operator -(const Matrix4& a_matrix) const;

	Matrix4& operator *=(const Matrix4& a_matrix);
	Matrix4& operator +=(const Matrix4& a_matrix);
	Matrix4& operator -=(const Matrix4& a_matrix);

	static Matrix4 LookAt(const Vector3& a_eye, const Vector3& a_at, const Vector3& a_up);
	static Matrix4 Perspective(float a_fov, float a_aspectRatio, float a_near, float a_far);
	static Matrix4 Inverse(const Matrix4& a_matrix);
	static void Decompose(const Matrix4& a_matrix, Vector3* a_translation, Vector3* a_rotation, Vector3* a_scale);
};