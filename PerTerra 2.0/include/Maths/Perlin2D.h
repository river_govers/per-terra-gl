#pragma once

class PerlinNoise2D
{
private:
	int* m_hash;

	float m_amplitude;
	float m_frequency;

	double Fade(double a_t) const;

	double Gradient(int a_hash, double a_x, double a_y) const;

	double NoiseNonVar(double a_x, double a_y, double a_amplitude, double a_frequency) const;
protected:

public:
	PerlinNoise2D(int a_seed, float a_amplitude, float a_frequency);
	~PerlinNoise2D();

	double Noise(double a_x, double a_y) const;
	double OctaveNoise(double a_x, double a_y, int a_octaves, double persistence) const;
};