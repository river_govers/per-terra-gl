#pragma once

#include <list>

#include "Maths.h"

class Transform
{
private:
	Transform* m_parent = nullptr;

	std::list<Transform*> m_children;

	/*Vector3 m_position;
	Vector3 m_forward;
	Vector3 m_up;
	Vector3 m_right;
	Vector3 m_scale;
	Vector3 m_euler;*/

	glm::mat4 m_matrix;
protected:

public:
	Transform();
	~Transform();

	void SetPosition(const Vector3& a_position);
	void Translate(const Vector3& a_translation);

	void SetRotation(const Vector3& a_euler);
	void Rotate(const Vector3& a_euler);

	/*Matrix4*/ glm::mat4 GetMatrix() const;
	void SetMatrix(const Matrix4& a_matrix);

	Vector3 Forward() const;
	Vector3 Up() const;
	Vector3 Right() const; 
};