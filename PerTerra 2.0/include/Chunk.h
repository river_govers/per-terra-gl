#pragma once

#include "Rendering\Models\Terrain.h"

#include <unordered_map>
#include <queue>
#define GLM_SWIZZLE
#include <glm\glm.hpp>

class Chunk;

struct ChunkLoadData
{
	const char* directory;
	Chunk* chunk;
	glm::vec2 position;
};

struct ChunkManagerIVec2Hash
{
	size_t operator()(const glm::vec2& a_vector);
};

class ChunkManager
{
private:
	static ChunkManager* mp_chunkManager;

	std::unordered_map<glm::ivec2, Chunk*, ChunkManagerIVec2Hash> mp_activeChunks;

	float m_chunkSize;
protected:

public:
	
};

class Chunk
{
private:
	Terrain* mp_terrain;
	glm::vec2 m_realPostion;
protected:

public:
	const Terrain& GetConstTerrain() const;
	Terrain& GetTerrain();
};