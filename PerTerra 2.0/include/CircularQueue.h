#pragma once

#include <atomic>

template<typename T>
class CircularQueue
{
private:
	unsigned int m_size;
	T* mp_data;

	std::atomic<unsigned int> m_readLocation;
	std::atomic<unsigned int> m_writeLocation;

	unsigned int Increment(unsigned int a_index)
	{
		return (a_index + 1) % m_size;
	}
protected:

public:
	CircularQueue()
	{
		m_readLocation = 0;
		m_writeLocation = 0;

		mp_data = new T[20];
		m_size = 20;
	}
	CircularQueue(const CircularQueue<T>& a_queue) = delete;
	CircularQueue(unsigned int a_size)
	{
		m_readLocation = 0;
		m_writeLocation = 0;

		mp_data = new T[a_size];

		m_size = a_size;
	}
	~CircularQueue()
	{
		delete[] mp_data;
	}

	bool Empty() const
	{
		return m_readLocation.load() == m_writeLocation.load();
	}
	bool Full() const
	{
		const unsigned int nextWriteLocation = Increment(m_writeLocation.load());

		return (nextWriteLocation == m_readLocation.load());
	}

	bool Push(const T& a_item)
	{
		unsigned int currentWriteLocation = m_writeLocation.load();
		unsigned int nextWriteLocation = Increment(currentWriteLocation);

		if (nextWriteLocation != m_readLocation.load())
		{
			mp_data[currentWriteLocation] = a_item;
			m_writeLocation.store(nextWriteLocation);

			return true;
		}

		return false;
	}
	bool Pop(T& a_item)
	{
		unsigned int currentReadLocation = m_readLocation.load();

		if (currentReadLocation != m_writeLocation.load())
		{
			a_item = mp_data[currentReadLocation];
			m_readLocation.store(Increment(currentReadLocation));

			return true;
		}

		return false;
	}
};
