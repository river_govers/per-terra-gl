#pragma once

#include "glm\glm.hpp"

class Camera;

class Frustrum
{
private:
	glm::vec4 m_planes[6];
protected:

public:
	Frustrum(const Camera& a_camera);

	bool CompareSphere(const glm::vec3& a_pos, float a_radius);
};