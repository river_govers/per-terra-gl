#pragma once

#define GLM_SWIZZLE 
#include "glm\glm.hpp"

#include "Maths\Transform.h"

class Texture;
class ShaderProgram;
class ParticleEmitter;

struct ParticleVert
{
	glm::vec4 position;
	glm::vec3 velocity;
	glm::vec4 color;
	glm::vec4 endColor;
	float size;
	float endSize;
	float spawnTime;
	float endTime;
};

struct Particle
{
	float size;
	float endSize;
	glm::vec4 color;
	glm::vec4 endColor;
	ParticleEmitter* emitter;
};

// TODO: Rewrite sub emmitters and fix
class ParticleEmitter
{
private:
	friend class Pipeline;

	Transform m_transform;

	double m_emissionTimer;
	double m_emissionRate;

	float m_minLifeSpan;
	float m_maxLifeSpan;

	float m_minVelocity;
	float m_maxVelocity;

	unsigned int m_maxParticles;
	unsigned int m_particleNum;

	ParticleVert* mp_particles;
	ParticleEmitter** mp_subEmitters;

	Particle m_particle;

	ShaderProgram* m_shader;

	unsigned int mp_vertexArrayObject, mp_vertexBufferObject;

	void InitBuffers();
protected:

public:
	ParticleEmitter(const ParticleEmitter& a_emitter);
	ParticleEmitter(unsigned int a_maxParticles, unsigned int a_emissionRate, float a_lifeTime, float a_velocity, const Particle& a_particle, ShaderProgram& a_shaderProgram);
	ParticleEmitter(unsigned int a_maxPartilces, unsigned int a_emissionRate, float a_minLifeTime, float a_maxLifeTime, float a_minVelocity, float a_maxVelocity, const Particle& a_particle, ShaderProgram& a_shaderProgram);
	~ParticleEmitter();

	void Update();

	void Clear();

	void SetMinVelocity(float a_velocity);
	void SetMaxVeloctiy(float a_velocity);

	void SetMinLifeSpan(float a_lifeSpan);
	void SetMaxLifeSpan(float a_lifeSpan);

	void SetEmmisionRate(unsigned int a_emissionRate);

	void SetParticle(const Particle& a_particle);

	void SetShader(ShaderProgram& a_shader);

	Transform& Transform();
};
