#pragma once

#include "Maths\Transform.h"

#include "Skinning\Render Target.h"

class Camera
{
private:
	friend class Pipeline;

	static Camera* m_primaryCamera;
	static Camera* m_drawingCamera;
	static std::list<Camera*> m_cameras;

	glm::mat4 m_view;
	glm::mat4 m_projection;

	RenderTarget* m_renderTarget;

	int m_clearFlags;

	Transform m_transform;
protected:

public:
	Camera(const float a_fov, const float a_near, const float a_far, const float a_aspectRatio, int a_clearFlags = 0b00000000000000000100000100000000);
	~Camera();

	Transform& Transform();

	glm::mat4 GetViewProjection() const;
	glm::mat4 Projection() const;
	glm::mat4 View() const;

	void SetRenderTarget(RenderTarget* const a_renderTarget);

	static std::list<Camera*>& GetCameraList();

	static void SetPrimaryCamera(Camera* const a_camera);
	static Camera* GetPrimaryCamera();
	static Camera* GetDrawingCamera();
};