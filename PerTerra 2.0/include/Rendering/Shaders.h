#pragma once

#include "Skinning\Texture.h"

#include <map>

class PixelShader
{
private:
	friend class ShaderProgram;

#if _OPEN_GL
	unsigned int mp_pixelShader;
#endif
protected:

public:
	PixelShader(const char* a_psSource);
	~PixelShader();

	static std::string LoadShader(const char* a_fileName);
};

class GeometryShader
{
private:
	friend class ShaderProgram;

	unsigned int mp_geometryShader;
protected:

public:
	GeometryShader(const char* a_gsSource);
	~GeometryShader();

	static std::string LoadShader(const char* a_fileName);
};

class VertexShader
{
private:
	friend class ShaderProgram;

#if _OPEN_GL
	unsigned int mp_vertShader;
#endif
protected:

public:
	VertexShader(const char* a_vsSource);
	~VertexShader();

	static std::string LoadShader(const char* a_fileName);
};

struct FloatUniform
{
	int uniformPos;
	float value;
};
struct Texture2DUniform
{
	int uniformPos;
	Texture* texture;
};

class ShaderProgram
{
private:
	friend class Pipeline;
	friend class GraphicUI;

#if _OPEN_GL
	unsigned int mp_programId;

	int m_projectionUniform;
	int m_viewUniform;
	int m_worldUniform;
	int m_lightCountUniform;
#endif

	std::map<const char*, Texture2DUniform> m_uniformTexture2Ds;
	std::map<const char*, FloatUniform> m_uniformFloats;
protected:

public:
	ShaderProgram(const PixelShader& a_pixelShader, const VertexShader& a_vertShader);
	ShaderProgram(const PixelShader& a_pixelShader, const GeometryShader& a_geometryShader, const VertexShader& a_vertShader);
	~ShaderProgram();

	void SetTexture(const char* a_varName, Texture* const a_texture);
	void SetFloat(const char* a_varName, float a_value);
};