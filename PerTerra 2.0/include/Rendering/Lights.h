#pragma once

#include "Skinning\Render Target.h"

#include "glm\glm.hpp"
#include "glm\ext.hpp"

#include <list>

#define MAX_LIGHTS 16

enum class e_LightType
{
	Null,
	First,
	Directional = First
};

class DirectionalLight;

class Light
{
private:
	// Allow the pipeline access to the render target
	friend class Pipeline;

	static std::list<Light*> mp_lights;
	static Light* m_drawingLight;
	static int m_lightCount;

	RenderTarget* mp_renderTarget;
	
	glm::vec4 m_lightColor;
protected:
	e_LightType m_lightType;

	glm::mat4 m_lightView; 
	glm::mat4 m_lightProjection;

	virtual void CalculateViewProjection() = 0;
public:
	Light();
	virtual ~Light();

	static std::list<Light*>& GetLightList();
	static Light* GetDrawingLight();
	static int GetLightCount();

	glm::mat4 GetViewMatrix() const;
	glm::mat4 GetProjectionMatrix() const;

	void SetLightColor(const glm::vec4& a_color);
};

class DirectionalLight : public Light
{
private:
	glm::vec3 m_lightDir;
protected:
	void CalculateViewProjection();
public:
	DirectionalLight();
	~DirectionalLight();

	void SetLightDirection(const glm::vec3& a_lightDir);
	glm::vec3 GetLightDirection() const;
};