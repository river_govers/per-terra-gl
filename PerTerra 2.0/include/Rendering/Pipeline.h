#pragma once

class ShaderProgram;
class Model;
class Terrain;
class ParticleEmitter;
class Texture;
class Skybox;
class Camera;

#include "Lights.h"

#include "Maths/Maths.h"

#if _DIRECT_X
#include "D3D11.h"
#include "D3DX11.h"
#include "D3DX10.h"

#ifdef _DEBUG
#include <dxgidebug.h>
#endif // _DEBUG
#endif

enum class e_DrawMode
{
	Null,
	Shadow,
	Standard
};

struct LightSStruct
{
	int type;
	glm::mat4 matrix;
	glm::vec3 pos;
	glm::vec3 dir;
	glm::vec4 color;
	Texture* shadowMap;
};

class Pipeline
{
private:
	friend class Application;

	static Pipeline* m_pipeline;

#if _DIRECT_X
	IDXGISwapChain* mp_swapChain = nullptr;
	ID3D11Device* mp_device = nullptr;
	ID3D11DeviceContext* mp_deviceContext = nullptr;

	ID3D11RenderTargetView* mp_backBuffer = nullptr;
	ID3D11Texture2D* mp_depthStencil = nullptr;
	ID3D11DepthStencilView* mp_depthStencilView = nullptr;

#if _DEBUG
	ID3D11Debug* mp_d3dDebug = nullptr;
#endif

	Pipeline(const HWND& a_window, int a_windowWidth, int a_windowHeight);
#endif
#if _OPEN_GL
	Pipeline();
#endif

	LightSStruct m_lights[MAX_LIGHTS];

	e_DrawMode m_drawMode;

	~Pipeline();
	
	static void OpenPipeline();
	static void DrawingCycle(Application* a_application);
	static void ClosePipeline();
protected:

public:
	static void DrawModel(const glm::mat4& a_transform, const ShaderProgram& a_shaderProgram, const Model& a_model);
	static void DrawModelCulled(const glm::mat4& a_transform, const ShaderProgram& a_shaderProgram, const Model& a_model);
	static void DrawParticleSystem(const ParticleEmitter& a_particleSystem);
	static void DrawTerrain(const glm::mat4& a_transform, const ShaderProgram& a_shaderProgram, const Terrain& a_terrain);
	static void DrawSkybox(const ShaderProgram& a_shaderProgram, const Skybox& a_skybox);
	static void DrawDirectionalLight(const ShaderProgram& a_shaderProgram, const DirectionalLight& a_directionalLight);

	static e_DrawMode GetDrawMode();
};