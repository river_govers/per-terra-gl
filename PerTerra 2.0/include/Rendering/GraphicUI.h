#pragma once

#include "Shaders.h"

#define GLM_SWIZZLE
#include "glm\glm.hpp"

class GraphicUI
{
private:
	friend class Application;

	static void Init();
	static void Destroy();

	static void OpenPipeline();
	static void ClosePipeline();
protected:

public:
	static void DrawString(const char* a_text, const glm::vec2& a_postion, const glm::vec2& a_scale, int a_fontSize, const glm::vec4& a_color, const Font& a_font, const ShaderProgram& a_shader, const glm::mat4& a_projection);
};