#pragma once

#include <ft2build.h>
#include FT_FREETYPE_H

#define GLM_SWIZZLE
#include "glm\glm.hpp"

#include "Texture.h"

struct FontVertex
{
	glm::vec2 position;
	glm::vec2 texCoord;
};

class Font
{
private:
	friend class GraphicUI;

	static FT_Library* m_ftLib;

	FT_Face m_face;

	Texture* mp_texture;

	unsigned int mp_vertAttribObject;
	unsigned int mp_vertBufferObject;
protected:

public:
	Font(const char* a_fileName);
	~Font();
};