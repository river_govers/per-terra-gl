#pragma once

class Texture
{
private:
	// Allow the pipeline to draw the texture
	friend class Pipeline;
	// Allow the render target to initailize the texture
	friend class RenderTarget;
	// Allow the font to be stored in a texture as a custom setup
	friend class Font;
	// Allow the GraphicUI to acces texture
	friend class GraphicUI;
	// Allow the cubemap to store its faces
	friend class Skybox;

	int m_imageWidth;
	int m_imageHeight;
	int m_imageFormat;

	unsigned int mp_texture;
	
	Texture();
protected:

public:
	~Texture();

	static void LoadFile(const char* a_filename, Texture*& a_texture);
	static void NullTexture(Texture*& a_texture);
	static void WhiteTexture(Texture*& a_texture);
	static void BlackTexture(Texture*& a_texture);
};
