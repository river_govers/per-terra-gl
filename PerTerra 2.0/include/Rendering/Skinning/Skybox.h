#pragma once

#include "Rendering\Skinning\Texture.h"

class Skybox
{
private:
	friend class Pipeline;

	Texture* mp_faces;
protected:

public:
	Skybox(const char* const a_faceFileNames[6]);
	~Skybox();
};