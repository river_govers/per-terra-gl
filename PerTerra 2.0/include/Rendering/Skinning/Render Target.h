#pragma once

#include "Texture.h"

struct Viewport
{
	unsigned int x;
	unsigned int y;
	unsigned int width;
	unsigned int height;
};

class RenderTarget
{
private:
	friend class Pipeline;

	Viewport m_viewPort;

	Texture* mp_textureBuffer;
	unsigned int mp_frameBufferObject;
protected:

public:
	RenderTarget(const Viewport& a_viewport, unsigned int a_internalTexFormat, unsigned int a_format, unsigned int a_attachment, unsigned int a_drawBufferMode = 0x0408);
	~RenderTarget();

	Texture& GetTexture();
};