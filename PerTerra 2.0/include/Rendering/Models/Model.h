#pragma once

#if _OPEN_GL
#define GLM_SWIZZLE 
#include "glm\glm.hpp"
#endif

struct Vertex
{
#if _OPEN_GL
	glm::vec4 position;
	glm::vec3 normal;
	glm::vec2 texCoord;
	glm::vec4 tangent;
	glm::vec4 biTangent;
#endif

	bool operator ==(const Vertex& a_vert) const;
};

class Model
{
private:
	friend class Pipeline;

	unsigned int mp_vertBuffer;
	unsigned int mp_indiceBuffer;
	unsigned int mp_vertArrayObject;

	float m_boundingRadius;

	unsigned short m_indicies;
protected:

public:
	~Model();

	static void GenerateQuad(Model*& a_model);
	static void GenerateIcoSphere(Model*& a_model, int a_recusion);

	static void LoadOBJ(const char* a_fileName, Model**& a_model, int& a_size);
};