#pragma once

#define GLM_SWIZZLE
#include <glm\glm.hpp>

struct TerrainTile
{
	bool state;
};

struct TerrainVertex
{
	glm::vec4 pos;
	glm::vec2 texCoord;

	bool operator ==(const TerrainVertex& a_vert);
};

struct TerrainTriangle
{
	TerrainVertex vertex[3];

	bool operator ==(const TerrainTriangle& a_triangle);
};

class Terrain
{
private:
	friend class Pipeline;

	// Tile data
	TerrainTile* mp_terrainTile;

	unsigned short m_res;
	float m_spacing;

	unsigned int m_indices;

	// Mesh data
	unsigned int mp_vertBuffer = -1;
	unsigned int mp_indiceBuffer;
	unsigned int mp_vertArrayObject;

	glm::vec3 VertexLerp(glm::vec3 a_vertA, glm::vec3 a_vertB, float a_isoLevel, float a_vertAVal, float a_vertBVal);
	bool TileCheck(unsigned short a_xA, unsigned short a_yA, unsigned short a_zA, unsigned short a_xB, unsigned short a_yB, unsigned short a_zB) const;
protected:

public:
	Terrain(unsigned short a_res, float a_spacing);
	~Terrain();

	TerrainTile& TerrainData(unsigned short a_x, unsigned short a_y, unsigned short a_z);

	void UpdateMesh(float a_isoLevel);

	unsigned short GetResolution() const;
	float GetSpacing() const;
};