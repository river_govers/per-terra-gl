#pragma once

struct GLFWwindow;

class Camera;

#if _DIRECT_X || (defined(_WIN32) && defined(_DEBUG))
#include <Windows.h>
#endif

class Application
{
private:
	static Application* mp_activeApplication;

#if _OPEN_GL
	GLFWwindow* mp_window;

	int m_majorVer;
	int m_minorVer;
#endif

#if _DIRECT_X
	HWND m_window;
#endif

	int m_windowXPos;
	int m_windowYPos;

	int m_windowWidth;
	int m_windowHeight;
protected:

public:
#if _OPEN_GL
	Application(const int a_windowWidth, const int a_windowHeight);
#endif

#if _DIRECT_X
	Application(int a_screenWidth, int a_screenHeight, const HINSTANCE& a_instance, int a_nCmdShow, const char* a_applicationName);
#endif

	virtual ~Application();

	static int GetWindowWidth();
	static int GetWindowHeight();

	void Run();

	virtual void Update() = 0;
	virtual void ShadowDraw() = 0;
	virtual void Draw() = 0;
	virtual void GUIDraw() = 0;
};

#if _DIRECT_X
LRESULT CALLBACK WindowProc(HWND a_hwnd, UINT a_message, WPARAM a_wParam, LPARAM a_lParam);
#endif