#include "Chunk.h"

ChunkManager* ChunkManager::mp_chunkManager = nullptr;

const Terrain & Chunk::GetConstTerrain() const
{
	return *mp_terrain;
}

Terrain& Chunk::GetTerrain()
{
	return *mp_terrain;
}

size_t ChunkManagerIVec2Hash::operator()(const glm::vec2 & a_vector)
{
	return 32768 * a_vector.x + a_vector.y;
}
