#include "Application.h"

#if _OPEN_GL
#include "gl_core_4_4.h"
#include "GLFW\glfw3.h"
#endif

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#define DEBGUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBGUG_NEW
#endif 

#include "TimeG.h"

#include "Rendering\Pipeline.h"
#include "Rendering\GraphicUI.h"

#include "Input.h"


Application* Application::mp_activeApplication = nullptr;

#if _OPEN_GL
Application::Application(const int a_windowWidth, const int a_windowHeight) : m_windowWidth(a_windowWidth), m_windowHeight(a_windowHeight)
{
	GraphicUI::Init();

	if (!mp_activeApplication)
	{
		mp_activeApplication = this;
	}

	if (!glfwInit())
	{
#if defined(_WIN32) && defined(_DEBUG)
		MessageBox(NULL, "Init failed", "Open GL Error", MB_OK | MB_ICONERROR);
#endif
		return;
	}

	mp_window = glfwCreateWindow(m_windowWidth, a_windowHeight, "Per Terra GL", NULL, NULL);

	glfwMakeContextCurrent(mp_window);

	if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
	{
		glfwDestroyWindow(mp_window);
		glfwTerminate();

#if defined(_WIN32) && defined(_DEBUG)
		MessageBox(NULL, "Function loading failed", "Open GL Error", MB_OK | MB_ICONERROR);
#endif

		return;
	}

	m_majorVer = ogl_GetMajorVersion();
	m_minorVer = ogl_GetMinorVersion();

	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glEnable(GL_DEPTH_TEST);

	TimeG::m_time = new TimeG();
	TimeG::m_time->m_timeScale = 1.0f;

	InputManager::mp_inputManager = new InputManager();
}
#endif

#if _DIRECT_X
Application::Application(int a_screenWidth, int a_screenHeight, const HINSTANCE & a_instance, int a_nCmdShow, const char * a_applicationName)
{
	mp_activeApplication = this;

	WNDCLASSEX windowClass;
	ZeroMemory(&windowClass, sizeof(WNDCLASSEX));

	windowClass.cbSize = sizeof(WNDCLASSEX);
	windowClass.style = CS_HREDRAW | CS_VREDRAW;
	windowClass.lpfnWndProc = WindowProc;
	windowClass.hInstance = a_instance;
	windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	windowClass.lpszClassName = a_applicationName;

	RegisterClassEx(&windowClass);

	RECT windowRect = { 0, 0, a_screenWidth, a_screenHeight };

	AdjustWindowRect(&windowRect, WS_OVERLAPPEDWINDOW, FALSE);

	m_windowXPos = windowRect.left;
	m_windowYPos = windowRect.bottom;
	m_windowWidth = windowRect.right - windowRect.left;
	m_windowHeight = windowRect.bottom - windowRect.top;

	m_window = CreateWindowEx(NULL,
							  a_applicationName,
							  a_applicationName,
							  WS_OVERLAPPEDWINDOW,
							  m_windowXPos,
							  m_windowYPos,
							  m_windowWidth,
							  m_windowHeight,
							  NULL,
							  NULL,
							  a_instance,
							  NULL);

	ShowWindow(m_window, a_nCmdShow);

	TimeG::m_time = new TimeG();
	TimeG::m_time->m_timeScale = 1.0f;
}
#endif

Application::~Application()
{
#if _OPEN_GL
	glfwDestroyWindow(mp_window);
	glfwTerminate();
#endif

	GraphicUI::Destroy();

	delete TimeG::m_time;

	delete InputManager::mp_inputManager;

	if (mp_activeApplication == this)
	{
		mp_activeApplication = nullptr;
	}
}

int Application::GetWindowWidth()
{
	return mp_activeApplication->m_windowWidth;
}

int Application::GetWindowHeight()
{
	return mp_activeApplication->m_windowHeight;
}

void Application::Run()
{
#if _OPEN_GL
	Pipeline* pipeline = new Pipeline();
	
	double currTime;
	double prevTime = glfwGetTime();
	TimeG::m_time->m_timePassed = 0.0f;

	while (!glfwWindowShouldClose(mp_window))
	{
		glfwPollEvents();
		
		InputManager::Update();

		currTime = glfwGetTime();
		TimeG::m_time->m_deltaTime = currTime - prevTime;
		prevTime = currTime;

		TimeG::m_time->m_timePassed += TimeG::m_time->m_deltaTime;

		Update();	

		Pipeline::OpenPipeline();

		Pipeline::DrawingCycle(this);

		Pipeline::ClosePipeline();

		GraphicUI::OpenPipeline();

		GUIDraw();

		GraphicUI::ClosePipeline();

		glfwSwapBuffers(mp_window);
	}

	delete pipeline;
#endif

#if _DIRECT_X
	Pipeline* pipeline = new Pipeline(m_window, m_windowWidth, m_windowHeight);

	MSG msg;

	ULONGLONG timeStart = GetTickCount64();
	ULONGLONG timeCur;

	while (true)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			bool handled = false;

			switch (msg.message)
			{
			case WM_QUIT:
			{
				delete pipeline;

				return;
			}
			}

			if (!handled)
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			timeCur = GetTickCount64();
			TimeG::m_time->m_deltaTime = (timeCur - timeStart) / 1000.0;
			timeStart = timeCur;

			Update();

			Draw();
		}
	}

	delete pipeline;
#endif // _DIRECT_X
}

#if _DIRECT_X
LRESULT CALLBACK WindowProc(HWND a_hwnd, UINT a_message, WPARAM a_wParam, LPARAM a_lParam)
{
	switch (a_message)
	{
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		return 0;
	}
	}

	return DefWindowProc(a_hwnd, a_message, a_wParam, a_lParam);
}
#endif
