#include "Rendering\Skinning\Render Target.h"

#include "gl_core_4_4.h"

RenderTarget::RenderTarget(const Viewport& a_viewport, unsigned int a_internalTexFormat, unsigned int a_format, unsigned int a_attachment, unsigned int a_drawBufferMode)
{
	glGenFramebuffers(1, &mp_frameBufferObject);
	glBindFramebuffer(GL_FRAMEBUFFER, mp_frameBufferObject);

	m_viewPort = a_viewport;

	mp_textureBuffer = new Texture();
	mp_textureBuffer->m_imageWidth = a_viewport.width;
	mp_textureBuffer->m_imageHeight = a_viewport.height;
	glGenTextures(1, &mp_textureBuffer->mp_texture);
	glBindTexture(GL_TEXTURE_2D, mp_textureBuffer->mp_texture);

	glTexImage2D(GL_TEXTURE_2D, 0, a_internalTexFormat, a_viewport.width, a_viewport.height, 0, a_format, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glFramebufferTexture(GL_FRAMEBUFFER, a_attachment, mp_textureBuffer->mp_texture, 0);

	glDrawBuffer(a_drawBufferMode);
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
#if defined(_WIN32) && defined(_DEBUG)
		MessageBox(NULL, "Frame buffer failed to initialise", "Open GL Error", MB_OK | MB_ICONERROR);
#endif
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}
RenderTarget::~RenderTarget()
{
	glDeleteFramebuffers(1, &mp_frameBufferObject);

	delete mp_textureBuffer;
}

Texture& RenderTarget::GetTexture()
{
	return *mp_textureBuffer;
}
