#include "Rendering\Skinning\Font.h"

FT_Library* Font::m_ftLib = nullptr;

#if defined(_WIN32) && defined(_DEBUG)
#include <Windows.h>
#endif

#include "gl_core_4_4.h"

Font::Font(const char* a_fileName)
{
	if (FT_New_Face(*m_ftLib, a_fileName, 0, &m_face))
	{
#if defined(_WIN32) && defined(_DEBUG)
		MessageBox(NULL, "Font loading failed", "Freetype", MB_OK | MB_ICONERROR);
#endif
	}

	mp_texture = new Texture();

	glGenTextures(1, &mp_texture->mp_texture);
	glBindTexture(GL_TEXTURE_2D, mp_texture->mp_texture);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glGenBuffers(1, &mp_vertBufferObject);
	glGenVertexArrays(1, &mp_vertAttribObject);
	glBindVertexArray(mp_vertAttribObject);
	
	glBindBuffer(GL_ARRAY_BUFFER, mp_vertBufferObject);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, FALSE, sizeof(FontVertex), (void**)offsetof(FontVertex, position));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, FALSE, sizeof(FontVertex), (void**)offsetof(FontVertex, texCoord));
}

Font::~Font()
{
	delete mp_texture;

	glDeleteVertexArrays(1, &mp_vertAttribObject);
	glDeleteBuffers(1, &mp_vertBufferObject);
}

