#include "Rendering\Skinning\Skybox.h"

#include "gl_core_4_4.h"

#include "Rendering\Skinning\Texture.h"

#include "stb_image.h"

Skybox::Skybox(const char* const a_faceFileNames[6])
{
	mp_faces = new Texture();
	
	glGenTextures(1, &mp_faces->mp_texture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, mp_faces->mp_texture);

	for (unsigned int i = 0; i < 6; ++i)
	{
		unsigned char* data = stbi_load(a_faceFileNames[i], &mp_faces->m_imageWidth, &mp_faces->m_imageHeight, &mp_faces->m_imageFormat, STBI_default);

		int format = 0;

		switch (mp_faces->m_imageFormat)
		{
		case 1:
		{
			format = GL_RED;
			break;
		}
		case 2:
		{
			format = GL_RG;
			break;
		}
		case 3:
		{
			format = GL_RGB;
			break;
		}
		case 4:
		{
			format = GL_RGBA;
			break;
		}
		}

		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, format, mp_faces->m_imageWidth, mp_faces->m_imageHeight, 0, format, GL_UNSIGNED_BYTE, data);

		stbi_image_free(data);
	}

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}

Skybox::~Skybox()
{
	delete mp_faces;
}
