#include "Rendering\Skinning\Texture.h"

#include "gl_core_4_4.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

Texture::Texture()
{
}

Texture::~Texture()
{
	glDeleteTextures(GL_TEXTURE_2D, &mp_texture);
}

void Texture::LoadFile(const char* a_filename, Texture*& a_texture)
{
	if (a_texture)
	{
		delete a_texture;
	}

	a_texture = new Texture();

	unsigned char* data = stbi_load(a_filename, &a_texture->m_imageWidth, &a_texture->m_imageHeight, &a_texture->m_imageFormat, STBI_default);

	int format = 0;

	switch (a_texture->m_imageFormat)
	{
	case 1:
	{
		format = GL_RED;
		break;
	}
	case 2:
	{
		format = GL_RG;
		break;
	}
	case 3:
	{
		format = GL_RGB;
		break;
	}
	case 4:
	{
		format = GL_RGBA;
		break;
	}
	}

	glGenTextures(1, &a_texture->mp_texture);
	glBindTexture(GL_TEXTURE_2D, a_texture->mp_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, format, a_texture->m_imageWidth, a_texture->m_imageHeight, 0, format, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	stbi_image_free(data);
}

void Texture::NullTexture(Texture*& a_texture)
{
	if (a_texture)
	{
		delete a_texture;
	}

	a_texture = new Texture();

	a_texture->m_imageWidth = 2;
	a_texture->m_imageHeight = 2;

	unsigned char data[12] = { 255, 0, 255, 
							   0,   0, 0,  
							   0,   0, 0,     
							   255, 0, 255 };

	glGenTextures(1, &a_texture->mp_texture);
	glBindTexture(GL_TEXTURE_2D, a_texture->mp_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, a_texture->m_imageWidth, a_texture->m_imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
}

void Texture::WhiteTexture(Texture*& a_texture)
{
	if (a_texture)
	{
		delete a_texture;
	}

	a_texture = new Texture();

	a_texture->m_imageWidth = 1;
	a_texture->m_imageHeight = 1;

	unsigned char data[3] = { 255, 255, 255 };

	glGenTextures(1, &a_texture->mp_texture);
	glBindTexture(GL_TEXTURE_2D, a_texture->mp_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, a_texture->m_imageWidth, a_texture->m_imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
}

void Texture::BlackTexture(Texture*& a_texture)
{
	if (a_texture)
	{
		delete a_texture;
	}

	a_texture = new Texture();

	a_texture->m_imageWidth = 1;
	a_texture->m_imageHeight = 1;

	unsigned char data[3] = { 0, 0, 0 };

	glGenTextures(1, &a_texture->mp_texture);
	glBindTexture(GL_TEXTURE_2D, a_texture->mp_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, a_texture->m_imageWidth, a_texture->m_imageHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
}
