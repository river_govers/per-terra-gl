#include "Rendering\Pipeline.h"

#include "Rendering\Shaders.h"
#include "Rendering\Camera.h"
#include "Rendering\Particle System.h"
#include "Rendering\Models\Model.h"
#include "Rendering\Models\Terrain.h"
#include "Rendering\Skinning\Skybox.h"

#include "Maths\Transform.h"

#include "TimeG.h"

#include "Application.h"

#include "Frustrum.h"

#if _OPEN_GL
#include "gl_core_4_4.h"

#include "glm\ext.hpp"
#endif

#include <string>
#include <iostream>
#include <algorithm>

#include <comdef.h>

Pipeline* Pipeline::m_pipeline = nullptr;

#ifndef min
#define min(x, y) (((x) < (y)) ? (x) : (y))
#endif // !min
#ifndef max
#define max(x, y) (((x) > (y)) ? (x) : (y))
#endif // !max(x, y) 

#if _OPEN_GL
Pipeline::Pipeline()
{
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);

	m_pipeline = this;

	m_drawMode = e_DrawMode::Null;
}
#endif

#if _DIRECT_X
Pipeline::Pipeline(const HWND& a_window, int a_windowWidth, int a_windowHeight)
{
	HRESULT hr;

#pragma region Init GPU
	UINT createDeviceFlags = 0;

#if _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	DXGI_SWAP_CHAIN_DESC scd;
	ZeroMemory(&scd, sizeof(DXGI_SWAP_CHAIN_DESC));

	scd.BufferCount = 1;
	scd.BufferDesc.Width = a_windowWidth;
	scd.BufferDesc.Height = a_windowHeight;
	scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	scd.BufferDesc.RefreshRate.Numerator = 60;
	scd.BufferDesc.RefreshRate.Denominator = 1;
	scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	scd.OutputWindow = a_window;
	scd.SampleDesc.Count = 1;
	scd.SampleDesc.Quality = 0;
	scd.Windowed = TRUE;
	scd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	D3D_DRIVER_TYPE drivers[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_REFERENCE,
		D3D_DRIVER_TYPE_SOFTWARE,
		D3D_DRIVER_TYPE_WARP
	};

	for (int i = 0; i < ARRAYSIZE(drivers); ++i)
	{
		hr = D3D11CreateDeviceAndSwapChain(NULL,
										   drivers[i],
										   NULL,
										   createDeviceFlags,
										   NULL,
										   NULL,
										   D3D11_SDK_VERSION,
										   &scd,
										   &mp_swapChain,
										   &mp_device,
										   NULL,
										   &mp_deviceContext);

		if (SUCCEEDED(hr))
		{
			break;
		}
	}

	if (FAILED(hr))
	{
		_com_error err = _com_error(hr);

		std::string string = "Failed to create device: ";

		string.append(err.ErrorMessage());

		MessageBox(NULL, string.c_str(), "DirectX Error", MB_OK | MB_ICONERROR);
		return;
	}
#pragma endregion

#pragma region Set render target
	ID3D11Texture2D* p_backBuffer;

	hr = mp_swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&p_backBuffer);
	if (FAILED(hr))
	{
		_com_error err = _com_error(hr);

		std::string string = "Back buffer fetch failed: ";

		string.append(err.ErrorMessage());

		MessageBox(NULL, string.c_str(), "DirectX Error", MB_OK | MB_ICONERROR);
	}

	hr = mp_device->CreateRenderTargetView(p_backBuffer, NULL, &mp_backBuffer);
	p_backBuffer->Release();
	if (FAILED(hr))
	{
		_com_error err = _com_error(hr);

		std::string string = "Back buffer creation failed: ";

		string.append(err.ErrorMessage());

		MessageBox(NULL, string.c_str(), "DirectX Error", MB_OK | MB_ICONERROR);
	}
#pragma endregion

#pragma region Create Depth Stencil
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(D3D11_TEXTURE2D_DESC));

	descDepth.Width = a_windowWidth;
	descDepth.Height = a_windowHeight;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;

	hr = mp_device->CreateTexture2D(&descDepth, NULL, &mp_depthStencil);
	if (FAILED(hr))
	{
		_com_error err = _com_error(hr);

		std::string string = "Failed to create depth stencil: ";

		string.append(err.ErrorMessage());

		MessageBox(NULL, string.c_str(), "DirectX Error", MB_OK | MB_ICONERROR);

		return;
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));

	descDSV.Format = descDepth.Format;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;

	hr = mp_device->CreateDepthStencilView(mp_depthStencil, &descDSV, &mp_depthStencilView);
	if (FAILED(hr))
	{
		_com_error err = _com_error(hr);

		std::string string = "Depth stencil view creation failed: ";

		string.append(err.ErrorMessage());

		MessageBox(NULL, string.c_str(), "DirectX Error", MB_OK | MB_ICONERROR);

		return;
	}
#pragma endregion

	mp_camera = new Camera(DirectX::XM_PI * 0.75f, 0.1f, 1000.0f, a_windowWidth / (float)a_windowHeight);
}
#endif

Pipeline::~Pipeline()
{
	m_pipeline = nullptr;

	for (auto iter = Camera::GetCameraList().begin(); iter != Camera::GetCameraList().end(); )
	{
		if (*iter)
		{
			delete *iter;

			iter = Camera::GetCameraList().begin();
		}
		else
		{
			++iter;
		}
	}

	for (auto iter = Light::GetLightList().begin(); iter != Light::GetLightList().end(); )
	{
		if (*iter)
		{
			delete *iter;

			iter = Light::GetLightList().begin();
		}
		else
		{
			++iter;
		}
	}

#if _DIRECT_X
	mp_swapChain->SetFullscreenState(FALSE, NULL);

	if (mp_swapChain)
	{
		mp_swapChain->Release();
	}
	if (mp_deviceContext)
	{
		mp_deviceContext->Release();
	}

	if (mp_backBuffer)
	{
		mp_backBuffer->Release();
	}
	if (mp_depthStencil)
	{
		mp_depthStencil->Release();
	}
	if (mp_depthStencilView)
	{
		mp_depthStencilView->Release();
	}
	if (mp_device)
	{
		mp_device->QueryInterface(__uuidof(ID3D11Debug), reinterpret_cast<void**>(&mp_d3dDebug));

		if (mp_d3dDebug)
		{
			mp_d3dDebug->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);

			mp_d3dDebug->Release();
		}

		mp_device->Release();
	}
#endif
}

void Pipeline::OpenPipeline()
{
	std::list<Camera*>& cameras = Camera::GetCameraList();

	for (auto iter = cameras.begin(); iter != cameras.end(); ++iter)
	{
		(*iter)->m_view = glm::inverse((*iter)->Transform().GetMatrix());
	}
}

const glm::mat4 textureSpaceOffset = { 0.5f, 0.0f, 0.0f, 0.0f,
									   0.0f, 0.5f, 0.0f, 0.0f,
									   0.0f, 0.0f, 0.5f, 0.0f,
									   0.5f, 0.5f, 0.5f, 1.0f };

void Pipeline::DrawingCycle(Application * a_application)
{
	std::list<Camera*>& cameras = Camera::GetCameraList();
	std::list<Light*>& lights = Light::GetLightList();

	m_pipeline->m_drawMode = e_DrawMode::Shadow;

	int index = 0;
	for (auto iter = lights.begin(); iter != lights.end(); ++iter)
	{
		(*iter)->CalculateViewProjection();

		if (index >= MAX_LIGHTS)
		{
			break;
		}

		LightSStruct& lightS = m_pipeline->m_lights[index];

		lightS.type = (int)(*iter)->m_lightType;
		lightS.color = (*iter)->m_lightColor;
		lightS.matrix = (*iter)->m_lightProjection * (*iter)->m_lightView;

		switch ((*iter)->m_lightType)
		{
		case e_LightType::Directional:
		{
			DirectionalLight* light = (DirectionalLight*)*iter;

			lightS.dir = light->GetLightDirection();

			break;
		}
		}

		Light::m_drawingLight = *iter;

		Viewport& viewport = (*iter)->mp_renderTarget->m_viewPort;

		glBindFramebuffer(GL_FRAMEBUFFER, (*iter)->mp_renderTarget->mp_frameBufferObject);
		glViewport(viewport.x, viewport.y, viewport.width, viewport.height);
		glClear(GL_DEPTH_BUFFER_BIT);

		a_application->ShadowDraw();

		lightS.shadowMap = (*iter)->mp_renderTarget->mp_textureBuffer;
		++index;
	}

	for (auto iter = cameras.begin(); iter != cameras.end(); ++iter)
	{
		Camera::m_drawingCamera = *iter;

		m_pipeline->m_drawMode = e_DrawMode::Standard;

		if ((*iter)->m_renderTarget)
		{
			glBindFramebuffer(GL_FRAMEBUFFER, (*iter)->m_renderTarget->mp_frameBufferObject);
			Viewport viewPort = (*iter)->m_renderTarget->m_viewPort;
			glViewport(viewPort.x, viewPort.y, viewPort.width, viewPort.height);
		}
		else
		{
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			glViewport(0, 0, Application::GetWindowWidth(), Application::GetWindowHeight());
		}

		glClear((*iter)->m_clearFlags);

		a_application->Draw();

		m_pipeline->m_drawMode = e_DrawMode::Null;
	}

	Camera::m_drawingCamera = nullptr;
}

void Pipeline::ClosePipeline()
{
}

void Pipeline::DrawModel(const glm::mat4& a_transform, const ShaderProgram & a_shaderProgram, const Model & a_model)
{
#if _OPEN_GL
	glUseProgram(a_shaderProgram.mp_programId);

	int index = 0;

	switch (m_pipeline->m_drawMode)
	{
	case e_DrawMode::Shadow:
	{
		Light& drawingLight = *Light::m_drawingLight;

		unsigned int lightMatrix = glGetUniformLocation(a_shaderProgram.mp_programId, "lightMatrix");
		glUniformMatrix4fv(lightMatrix, 1, FALSE, &(drawingLight.m_lightProjection * drawingLight.m_lightView)[0].x);
		
		glUniformMatrix4fv(a_shaderProgram.m_worldUniform, 1, FALSE, &a_transform[0].x);

		break;
	}
	case e_DrawMode::Standard:
	{
		glUniformMatrix4fv(a_shaderProgram.m_projectionUniform, 1, FALSE, &Camera::GetDrawingCamera()->m_projection[0].x);
		glUniformMatrix4fv(a_shaderProgram.m_viewUniform, 1, FALSE, &Camera::GetDrawingCamera()->m_view[0].x);
		glUniformMatrix4fv(a_shaderProgram.m_worldUniform, 1, FALSE, &a_transform[0].x);
		glUniform1i(a_shaderProgram.m_lightCountUniform, Light::m_lightCount);

		LightSStruct* lights = m_pipeline->m_lights;

		for (int i = 0; i < min(MAX_LIGHTS, Light::m_lightCount); ++i)
		{
			glActiveTexture(GL_TEXTURE0 + index);
			glBindTexture(GL_TEXTURE_2D, lights[i].shadowMap->mp_texture);

			// Sets the type of the light
			int location = glGetUniformLocation(a_shaderProgram.mp_programId, (std::string("lights[") + std::to_string(i) + "].type").c_str());
			glUniform1i(	   location, lights[i].type);
			// Sets the light matrix
			location = glGetUniformLocation(a_shaderProgram.mp_programId, (std::string("lights[") + std::to_string(i) + "].matrix").c_str());
			glUniformMatrix4fv(location, 1, FALSE, &(textureSpaceOffset * lights[i].matrix)[0].x);
			// Sets the lights position
			/*location = glGetUniformLocation(a_shaderProgram.mp_programId, (std::string("lights[") + std::to_string(i) + "].pos").c_str());
			glUniform3f(	   location, lights[i].pos.x, lights[i].pos.y, lights[i].pos.z);*/
			// Sets the lights direction
			location = glGetUniformLocation(a_shaderProgram.mp_programId, (std::string("lights[") + std::to_string(i) + "].dir").c_str());
			glUniform3f(	   location, lights[i].dir.x, lights[i].dir.y, lights[i].dir.z);
			// Sets the lights color
			location = glGetUniformLocation(a_shaderProgram.mp_programId, (std::string("lights[") + std::to_string(i) + "].color").c_str());
			glUniform4f(	   location, lights[i].color.r, lights[i].color.g, lights[i].color.b, lights[i].color.a);
			// Sets the shadow map of the light
			location = glGetUniformLocation(a_shaderProgram.mp_programId, (std::string("lights[") + std::to_string(i) + "].shadowMap").c_str());
			glUniform1i(	   location, index++);		
		}

		break;
	}
	}

	for (auto iter = a_shaderProgram.m_uniformTexture2Ds.begin(); iter != a_shaderProgram.m_uniformTexture2Ds.end(); ++iter)
	{
		glActiveTexture(GL_TEXTURE0 + index);
		glBindTexture(GL_TEXTURE_2D, iter->second.texture->mp_texture);
		/*unsigned int loc = glGetUniformLocation(a_shaderProgram.mp_programId, iter->first);*/
		glUniform1i(iter->second.uniformPos, index++);
	}

	for (auto iter = a_shaderProgram.m_uniformFloats.begin(); iter != a_shaderProgram.m_uniformFloats.end(); ++iter)
	{
		/*unsigned int uniformFloat = glGetUniformLocation(a_shaderProgram.mp_programId, iter->first);*/
		glUniform1f(iter->second.uniformPos, iter->second.value);
	}

	glBindVertexArray(a_model.mp_vertArrayObject);

	glDrawElements(GL_TRIANGLES, a_model.m_indicies, GL_UNSIGNED_SHORT, NULL);
#endif
}
void Pipeline::DrawModelCulled(const glm::mat4& a_transform, const ShaderProgram& a_shaderProgram, const Model& a_model)
{
	glm::vec3 scale;
	glm::quat quat;
	glm::vec3 trans;
	glm::vec3 skew;
	glm::vec4 persp;

	glm::decompose(a_transform, scale, quat, trans, skew, persp);

	if (Frustrum(*Camera::GetDrawingCamera()).CompareSphere(glm::vec3(a_transform[3]), a_model.m_boundingRadius * max(scale.x, max(scale.y, scale.z))))
	{
		DrawModel(a_transform, a_shaderProgram, a_model);
	}
}
void Pipeline::DrawParticleSystem(const ParticleEmitter& a_particleSystem)
{
	switch (m_pipeline->m_drawMode)
	{
	case e_DrawMode::Shadow:
	{
		std::cout << "Warning: Drawing particle shadows unsupported" << std::endl;

		break;
	}
	case e_DrawMode::Standard:
	{
		if (a_particleSystem.m_particleNum)
		{
			glUseProgram(a_particleSystem.m_shader->mp_programId);

			glUniformMatrix4fv(a_particleSystem.m_shader->m_projectionUniform, 1, FALSE, &Camera::GetDrawingCamera()->m_projection[0].x);
			glUniformMatrix4fv(a_particleSystem.m_shader->m_viewUniform, 1, FALSE, &Camera::GetDrawingCamera()->m_view[0].x);

			int location = glGetUniformLocation(a_particleSystem.m_shader->mp_programId, "timePassed");
			glUniform1f(location, (float)TimeG::TimePassed());

			glBindVertexArray(a_particleSystem.mp_vertexArrayObject);
			glDrawArrays(GL_POINTS, 0, a_particleSystem.m_particleNum);
		}

		break;
	}
	}
}
void Pipeline::DrawTerrain(const glm::mat4& a_transform, const ShaderProgram& a_shaderProgram, const Terrain& a_terrain)
{
#if _OPEN_GL
	glUseProgram(a_shaderProgram.mp_programId);
	
	unsigned int index = 0;

	switch (m_pipeline->m_drawMode)
	{
	case e_DrawMode::Shadow:
	{
		Light& drawingLight = *Light::m_drawingLight;

		unsigned int lightMatrix = glGetUniformLocation(a_shaderProgram.mp_programId, "lightMatrix");
		glUniformMatrix4fv(lightMatrix, 1, FALSE, &((drawingLight.m_lightProjection * drawingLight.m_lightView))[0].x);

		glUniformMatrix4fv(a_shaderProgram.m_worldUniform, 1, FALSE, &a_transform[0].x);

		break;
	}
	case e_DrawMode::Standard:
	{
		glUniformMatrix4fv(a_shaderProgram.m_projectionUniform, 1, FALSE, &Camera::GetDrawingCamera()->m_projection[0].x);
		glUniformMatrix4fv(a_shaderProgram.m_viewUniform, 1, FALSE, &Camera::GetDrawingCamera()->m_view[0].x);
		glUniformMatrix4fv(a_shaderProgram.m_worldUniform, 1, FALSE, &a_transform[0].x);

		LightSStruct* lights = m_pipeline->m_lights;

		int location = glGetUniformLocation(a_shaderProgram.mp_programId, "lightCount");
		glUniform1i(location, Light::m_lightCount);

		for (int i = 0; i < min(MAX_LIGHTS, Light::m_lightCount); ++i)
		{
			glActiveTexture(GL_TEXTURE0 + index);
			glBindTexture(GL_TEXTURE_2D, lights[i].shadowMap->mp_texture);

			// Sets the type of the light
			location = glGetUniformLocation(a_shaderProgram.mp_programId, (std::string("lights[") + std::to_string(i) + "].type").c_str());
			glUniform1i(location, lights[i].type);
			// Sets the light matrix
			location = glGetUniformLocation(a_shaderProgram.mp_programId, (std::string("lights[") + std::to_string(i) + "].matrix").c_str());
			glUniformMatrix4fv(location, 1, FALSE, &(textureSpaceOffset * lights[i].matrix)[0].x);
			// Sets the lights position
			/*location = glGetUniformLocation(a_shaderProgram.mp_programId, (std::string("lights[") + std::to_string(i) + "].pos").c_str());
			glUniform3f(	   location, lights[i].pos.x, lights[i].pos.y, lights[i].pos.z);*/
			// Sets the lights direction
			location = glGetUniformLocation(a_shaderProgram.mp_programId, (std::string("lights[") + std::to_string(i) + "].dir").c_str());
			glUniform3f(location, lights[i].dir.x, lights[i].dir.y, lights[i].dir.z);
			// Sets the lights color
			location = glGetUniformLocation(a_shaderProgram.mp_programId, (std::string("lights[") + std::to_string(i) + "].color").c_str());
			glUniform4f(location, lights[i].color.r, lights[i].color.g, lights[i].color.b, lights[i].color.a);
			// Sets the shadow map of the light
			location = glGetUniformLocation(a_shaderProgram.mp_programId, (std::string("lights[") + std::to_string(i) + "].shadowMap").c_str());
			glUniform1i(location, index++);
		}

		break;
	}
	}

	for (auto iter = a_shaderProgram.m_uniformTexture2Ds.begin(); iter != a_shaderProgram.m_uniformTexture2Ds.end(); ++iter)
	{
		glActiveTexture(GL_TEXTURE0 + index);
		glBindTexture(GL_TEXTURE_2D, iter->second.texture->mp_texture);

		glUniform1i(iter->second.uniformPos, index++);
	}

	for (auto iter = a_shaderProgram.m_uniformFloats.begin(); iter != a_shaderProgram.m_uniformFloats.end(); ++iter)
	{
		glUniform1f(iter->second.uniformPos, iter->second.value);
	}

	glBindVertexArray(a_terrain.mp_vertArrayObject);
	glDrawElements(GL_TRIANGLES, a_terrain.m_indices, GL_UNSIGNED_INT, NULL);
#endif
}
void Pipeline::DrawSkybox(const ShaderProgram& a_shaderProgram, const Skybox & a_skybox)
{
	glDepthMask(FALSE);

	glUseProgram(a_shaderProgram.mp_programId);

	int location = glGetUniformLocation(a_shaderProgram.mp_programId, "invView");
	glUniformMatrix4fv(location, 1, FALSE, &Camera::m_drawingCamera->m_transform.GetMatrix()[0].x);
	location = glGetUniformLocation(a_shaderProgram.mp_programId, "invProjection");
	glUniformMatrix4fv(location, 1, FALSE, &glm::inverse(Camera::m_drawingCamera->m_projection)[0].x);
	location = glGetUniformLocation(a_shaderProgram.mp_programId, "cubeMap");
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, a_skybox.mp_faces->mp_texture);
	glUniform1i(location, 0);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glDepthMask(TRUE);
}
void Pipeline::DrawDirectionalLight(const ShaderProgram& a_shaderProgram, const DirectionalLight& a_directionalLight)
{
	glDepthMask(FALSE);

	/*glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
	glm::vec3 forward = a_directionalLight.GetLightDirection();

	if (glm::dot(up, forward) == 1.0f)
	{
		up = glm::vec3(0.0f, 0.0f, 1.0f);
	}
	else if (glm::dot(up, forward) == -1.0f)
	{
		up = glm::vec3(0.0f, 0.0f, -1.0f);
	}

	glm::vec3 right = glm::cross(up, forward);
	up = glm::cross(forward, right);*/

	glm::mat4 world = glm::mat4(0.25f);

	/*world[0] = glm::vec4(right, 0.0f);
	world[1] = glm::vec4(up, 0.0f);
	world[2] = glm::vec4(forward, 0.0f);*/

	world[3] = glm::vec4(-a_directionalLight.GetLightDirection(), 1.0f);

	glUseProgram(a_shaderProgram.mp_programId);
	glUniformMatrix4fv(a_shaderProgram.m_projectionUniform, 1, FALSE, &Camera::m_drawingCamera->m_projection[0].x);
	glUniformMatrix4fv(a_shaderProgram.m_viewUniform, 1, FALSE, &Camera::m_drawingCamera->m_view[0].x);
	glUniformMatrix4fv(a_shaderProgram.m_worldUniform, 1, FALSE, &world[0].x);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glDepthMask(TRUE);
}

e_DrawMode Pipeline::GetDrawMode()
{
	return m_pipeline->m_drawMode;
}
