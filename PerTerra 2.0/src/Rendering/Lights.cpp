#include "Rendering/Lights.h"

#include "gl_core_4_4.h"

#include <iostream>

std::list<Light*> Light::mp_lights = std::list<Light*>();
Light* Light::m_drawingLight = nullptr;
int Light::m_lightCount = 0;

Light::Light()
{
	++m_lightCount;
	mp_lights.push_back(this);

	if (m_lightCount > MAX_LIGHTS)
	{
		std::cout << "Warning: Number of lights exceed maximum!" << std::endl;
		std::cout << "Increase maximum lights or reduces lights in scene" << std::endl;
	}

	mp_renderTarget = new RenderTarget({ 0, 0, 1024, 1024 }, GL_DEPTH_COMPONENT16, GL_DEPTH_COMPONENT, GL_DEPTH_ATTACHMENT, GL_NONE);

	m_lightType = e_LightType::Null;
}

Light::~Light()
{
	--m_lightCount;
	mp_lights.remove(this);

	delete mp_renderTarget;
}

std::list<Light*>& Light::GetLightList()
{
	return mp_lights;
}

Light * Light::GetDrawingLight()
{
	return m_drawingLight;
}

int Light::GetLightCount()
{
	return m_lightCount;
}

glm::mat4 Light::GetViewMatrix() const
{
	return m_lightView;
}

glm::mat4 Light::GetProjectionMatrix() const
{
	return m_lightProjection;
}

void Light::SetLightColor(const glm::vec4 & a_color)
{
	m_lightColor = a_color;
}

void DirectionalLight::CalculateViewProjection()
{
	m_lightProjection = glm::ortho<float>(-10, 10, -10, 10, -10, 10);
	m_lightView = glm::lookAt(m_lightDir, glm::vec3(0.0f), { 0.0f, 1.0f, 0.0f });
}

DirectionalLight::DirectionalLight()
{
	m_lightType = e_LightType::Directional;

	m_lightDir = { 0.0f, 0.0f, 1.0f };
}
DirectionalLight::~DirectionalLight()
{

}
void DirectionalLight::SetLightDirection(const glm::vec3& a_lightDir)
{
	m_lightDir = a_lightDir;
}
glm::vec3 DirectionalLight::GetLightDirection() const
{
	return m_lightDir;
}
