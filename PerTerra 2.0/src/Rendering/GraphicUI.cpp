#include "Rendering\GraphicUI.h"

#include "Rendering\Skinning\Font.h"

#if defined(_WIN32) && defined(_DEBUG)
#include <Windows.h>
#endif

#include "gl_core_4_4.h"

#include "Application.h"

void GraphicUI::Init()
{
	Font::m_ftLib = new FT_Library();

	if (FT_Init_FreeType(Font::m_ftLib))
	{
#if defined(_WIN32) && defined(_DEBUG)
		MessageBox(NULL, "Initialistion failed", "Freetype", MB_OK | MB_ICONERROR);
#endif
	}
}

void GraphicUI::Destroy()
{
	delete Font::m_ftLib;
}

void GraphicUI::OpenPipeline()
{
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}
void GraphicUI::ClosePipeline()
{
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
}

void DrawCharacter(const FT_GlyphSlot& a_glyph, float a_x, float a_y, float a_width, float a_height)
{
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, a_glyph->bitmap.width, a_glyph->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE, a_glyph->bitmap.buffer);

	FontVertex vert[4] =
	{
		{ { a_x,			a_y },				{ 0.0f, 1.0f } },
		{ { a_x + a_width,	a_y },				{ 1.0f, 1.0f } },
		{ { a_x,			a_y + a_height },	{ 0.0f, 0.0f } },
		{ { a_x + a_width,	a_y + a_height },	{ 1.0f, 0.0f } }
	};

	glBufferData(GL_ARRAY_BUFFER, ARRAYSIZE(vert) * sizeof(FontVertex), vert, GL_DYNAMIC_DRAW);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void GraphicUI::DrawString(const char* a_text, const glm::vec2& a_postion, const glm::vec2& a_scale, int a_fontSize, const glm::vec4& a_color, const Font& a_font, const ShaderProgram& a_shader, const glm::mat4& a_projection)
{
	glm::vec2 position = a_postion;

	FT_Set_Pixel_Sizes(a_font.m_face, 0, a_fontSize);
	
	glUseProgram(a_shader.mp_programId);
	
	glActiveTexture(GL_TEXTURE20);
	glBindTexture(GL_TEXTURE_2D, a_font.mp_texture->mp_texture);
	unsigned int texUniform = glGetUniformLocation(a_shader.mp_programId, "font");
	glUniform1i(texUniform, 20);

	unsigned int fontColorUniform = glGetUniformLocation(a_shader.mp_programId, "fontColor");
	glUniform4f(fontColorUniform, a_color.r, a_color.g, a_color.b, a_color.a);

	glUniformMatrix4fv(a_shader.m_projectionUniform, 1, FALSE, &a_projection[0].x);
	
	glBindVertexArray(a_font.mp_vertAttribObject);
	glBindBuffer(GL_ARRAY_BUFFER, a_font.mp_vertBufferObject);

	for (const char* character = a_text; *character != 0; ++character)
	{
		if (FT_Load_Char(a_font.m_face, *character, FT_LOAD_RENDER))
		{
			continue;
		}

		FT_GlyphSlot glyph = a_font.m_face->glyph;
		
		switch (*character)
		{
		case 10:
		{
			position = { a_postion.x, position.y - ((a_fontSize * 1.20f) * a_scale.y) };
			
			continue;
		}
		case 32:
		{
			break;
		}
		// Slight Lower
		case 59:
		case 103:
		case 106:
		case 112:
		case 113:
		case 121:
		{
			float x2 = (position.x + glyph->bitmap_left) * a_scale.x;
			float y2 = ((position.y - glyph->bitmap_top) - (a_fontSize * 0.25f)) * a_scale.y;
			float w = glyph->bitmap.width * a_scale.x;
			float h = glyph->bitmap.rows * a_scale.y;

			DrawCharacter(glyph, x2, y2, w, h);

			break;
		}
		// Slight Lift
		case 66:
		case 67:
		case 68:
		case 82:
		case 98:
		case 116:
		{
			float x2 = (position.x + glyph->bitmap_left) * a_scale.x;
			float y2 = ((position.y - glyph->bitmap_top) + (a_fontSize * 0.1f)) * a_scale.y;
			float w = glyph->bitmap.width * a_scale.x;
			float h = glyph->bitmap.rows * a_scale.y;

			DrawCharacter(glyph, x2, y2, w, h);

			break;
		}
		case 104:
		case 105:
		// Mid lift
		{
			float x2 = (position.x + glyph->bitmap_left) * a_scale.x;
			float y2 = ((position.y - glyph->bitmap_top) + (a_fontSize * 0.2f)) * a_scale.y;
			float w = glyph->bitmap.width * a_scale.x;
			float h = glyph->bitmap.rows * a_scale.y;

			DrawCharacter(glyph, x2, y2, w, h);

			break;
		}
		// Major Lower
		case 44:
		case 46:
		case 95:
		{
			float x2 = (position.x + glyph->bitmap_left) * a_scale.x;
			float y2 = ((position.y - glyph->bitmap_top) - (a_fontSize * 0.5f)) * a_scale.y;
			float w = glyph->bitmap.width * a_scale.x;
			float h = glyph->bitmap.rows * a_scale.y;

			DrawCharacter(glyph, x2, y2, w, h);

			break;
		}
		// Major Lift
		case 34:
		case 39:
		case 42:
		case 94:
		{
			float x2 = (position.x + glyph->bitmap_left) * a_scale.x;
			float y2 = ((position.y - glyph->bitmap_top) + (a_fontSize * 0.5f)) * a_scale.y;
			float w = glyph->bitmap.width * a_scale.x;
			float h = glyph->bitmap.rows * a_scale.y;

			DrawCharacter(glyph, x2, y2, w, h);

			break;
		}
		default:
		{
			float x2 = (position.x + glyph->bitmap_left) * a_scale.x;
			float y2 = (position.y - glyph->bitmap_top) * a_scale.y;
			float w = glyph->bitmap.width * a_scale.x;
			float h = glyph->bitmap.rows * a_scale.y;

			DrawCharacter(glyph, x2, y2, w, h);

			break;
		}
		}

		position.x += (glyph->advance.x / 64) * a_scale.x;
		position.y += (glyph->advance.y / 64) * a_scale.y;
	}
}
