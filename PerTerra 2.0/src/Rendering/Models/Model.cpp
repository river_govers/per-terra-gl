#include "Rendering/Models/Model.h"

#if _OPEN_GL
#include "gl_core_4_4.h"
#endif

#include "Maths/Maths.h"

#include <vector>
#include <algorithm>

#include <fstream>

#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"

Model::~Model()
{
#if _OPEN_GL
	glDeleteBuffers(1, &mp_vertBuffer);
	glDeleteBuffers(1, &mp_indiceBuffer);
	glDeleteBuffers(1, &mp_vertArrayObject);
#endif
}

enum class e_indType
{
	VertexInd,
	VertexTexCoordInd,
	VertexNormalInd,
	VertexNormalNoTexCoordInd,
	QuadVertexInd,
	QuadVertexTexCoordInd,
	QuadVertexNormalInd,
	QuadVertexNormalNoTexCoordInd
};

void Model::GenerateQuad(Model*& a_model)
{
#if _OPEN_GL
	if (a_model)
	{
		delete a_model;
	}

	a_model = new Model();

	Vertex vertices[] = 
	{
		{ { -1.0f, 0.0f, -1.0f, 1.0f }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 1.0f } },
		{ { 1.0f,  0.0f, -1.0f, 1.0f }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 1.0f } },
		{ { 1.0f,  0.0f, 1.0f,  1.0f }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 0.0f } },
		{ { -1.0f, 0.0f, 1.0f,  1.0f }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 0.0f } }
	};

	unsigned short indices[] = { 3, 1, 0, 2, 1, 3 };
	//unsigned short indices[] = { 0, 1, 3, 3, 1, 2 };

	a_model->m_indicies = (unsigned short)ARRAYSIZE(indices);

	glGenBuffers(1, &a_model->mp_vertBuffer);
	glGenBuffers(1, &a_model->mp_indiceBuffer);
	glGenVertexArrays(1, &a_model->mp_vertArrayObject);
	glBindVertexArray(a_model->mp_vertArrayObject);
	
	glBindBuffer(GL_ARRAY_BUFFER, a_model->mp_vertBuffer);
	glBufferData(GL_ARRAY_BUFFER, ARRAYSIZE(vertices) * sizeof(Vertex), vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, a_model->mp_indiceBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, ARRAYSIZE(indices) * sizeof(unsigned short), indices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(0, 4, GL_FLOAT, FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, position));
	glVertexAttribPointer(1, 3, GL_FLOAT, FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));
	glVertexAttribPointer(2, 2, GL_FLOAT, FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texCoord));

	glBindVertexArray(NULL);
#endif
}

unsigned short GetMidPoint(unsigned short a_indicieA, unsigned short a_indicieB, std::vector<Vertex>& a_verticies)
{
#if _OPEN_GL
	glm::vec4 posA = a_verticies[(int)a_indicieA].position;
	glm::vec4 posB = a_verticies[(int)a_indicieB].position;
	glm::vec4 mid = glm::vec4(glm::normalize(glm::vec3((posA + posB).xyz) / 2.0f).xyz, 1.0f);

	for (int i = 0; i < (int)a_verticies.size(); ++i)
	{
		if (a_verticies[i].position == mid)
		{
			return (unsigned short)i;
		}
	}

	a_verticies.push_back({ mid, { mid.xyz }, { 0.0f, 1.0f } });

	return (unsigned short)a_verticies.size() - 1;
#endif
}

void Model::GenerateIcoSphere(Model *& a_model, int a_recusion)
{
#if _OPEN_GL
	std::vector<Vertex> vertices;
	vertices.reserve(12);

	float t = (1.0f + sqrt(0.5f)) / 2.0f;

	vertices.push_back({ { -1.0f, t,     0.0f,  1.0f }, { 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } });
	vertices.push_back({ { 1.0f,  t,     0.0f,  1.0f }, { 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } });
	vertices.push_back({ { -1.0f, -t,    0.0f,  1.0f }, { 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } });
	vertices.push_back({ { 1.0f,  -t,    0.0f,  1.0f }, { 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } });
														
	vertices.push_back({ { 0.0f,  -1.0f, t,     1.0f }, { 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } });
	vertices.push_back({ { 0.0f,  1.0f,  t,	    1.0f }, { 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } });
	vertices.push_back({ { 0.0f,  -1.0f, -t,    1.0f }, { 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } });
	vertices.push_back({ { 0.0f,  1.0f,  -t,    1.0f }, { 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } });
														
	vertices.push_back({ { t,	  0.0f,	 -1.0f, 1.0f }, { 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } });
	vertices.push_back({ { t,	  0.0f,  1.0f,  1.0f }, { 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } });
	vertices.push_back({ { -t,	  0.0f,  -1.0f, 1.0f }, { 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } });
	vertices.push_back({ { -t,	  0.0f,  1.0f,  1.0f }, { 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f } });

	for (auto iter = vertices.begin(); iter != vertices.end(); ++iter)
	{
		iter->position = glm::vec4(glm::normalize(glm::vec3(iter->position.xyz)), 1.0f);
		iter->normal = iter->position.xyz;
	}

	std::vector<unsigned short> indicies;

	indicies.reserve(60);

	indicies.push_back(0);  indicies.push_back(11); indicies.push_back(5);
	indicies.push_back(0);  indicies.push_back(5);  indicies.push_back(1);
	indicies.push_back(0);  indicies.push_back(1);  indicies.push_back(7);
	indicies.push_back(0);  indicies.push_back(7);  indicies.push_back(10);
	indicies.push_back(0);  indicies.push_back(10); indicies.push_back(11);

	indicies.push_back(1);  indicies.push_back(5);  indicies.push_back(9);
	indicies.push_back(5);  indicies.push_back(11); indicies.push_back(4);
	indicies.push_back(11); indicies.push_back(10); indicies.push_back(2);
	indicies.push_back(10); indicies.push_back(7);  indicies.push_back(6);
	indicies.push_back(7);  indicies.push_back(1);  indicies.push_back(8);

	indicies.push_back(3);  indicies.push_back(9);  indicies.push_back(4);
	indicies.push_back(3);  indicies.push_back(4);  indicies.push_back(2);
	indicies.push_back(3);  indicies.push_back(2);  indicies.push_back(6);
	indicies.push_back(3);  indicies.push_back(6);  indicies.push_back(8);
	indicies.push_back(3);  indicies.push_back(8);  indicies.push_back(9);

	indicies.push_back(4);  indicies.push_back(9);  indicies.push_back(5);
	indicies.push_back(2);  indicies.push_back(4);  indicies.push_back(11);
	indicies.push_back(6);  indicies.push_back(2);  indicies.push_back(10);
	indicies.push_back(8);  indicies.push_back(6);  indicies.push_back(7);
	indicies.push_back(9);  indicies.push_back(8);  indicies.push_back(1);

	for (int i = 0; i < a_recusion; ++i)
	{
		std::vector<unsigned short> tempFaces;
		tempFaces.reserve(indicies.size() * 4);

		for (int j = 0; j < (int)indicies.size(); j += 3)
		{
			unsigned short indicieA = indicies[j];
			unsigned short indicieB = indicies[j + 1];
			unsigned short indicieC = indicies[j + 2];

			unsigned short a = GetMidPoint(indicieA, indicieB, vertices);
			unsigned short b = GetMidPoint(indicieB, indicieC, vertices);
			unsigned short c = GetMidPoint(indicieC, indicieA, vertices);

			tempFaces.push_back(indicieA); tempFaces.push_back(a); tempFaces.push_back(c);
			tempFaces.push_back(indicieB); tempFaces.push_back(b); tempFaces.push_back(a);
			tempFaces.push_back(indicieC); tempFaces.push_back(c); tempFaces.push_back(b);
			tempFaces.push_back(a);		   tempFaces.push_back(b); tempFaces.push_back(c);
		}

		indicies = tempFaces;
	}

	if (a_model)
	{
		delete a_model;
	}

	a_model = new Model;

	a_model->m_indicies = (unsigned short)indicies.size();
	a_model->m_boundingRadius = 1.0f;

	glGenBuffers(1, &a_model->mp_vertBuffer);
	glGenBuffers(1, &a_model->mp_indiceBuffer);
	glGenVertexArrays(1, &a_model->mp_vertArrayObject);
	glBindVertexArray(a_model->mp_vertArrayObject);

	glBindBuffer(GL_ARRAY_BUFFER, a_model->mp_vertBuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(0, 4, GL_FLOAT, FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, position));
	glVertexAttribPointer(1, 3, GL_FLOAT, FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));
	glVertexAttribPointer(2, 2, GL_FLOAT, FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texCoord));

	a_model->m_indicies = (unsigned short)indicies.size();

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, a_model->mp_indiceBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicies.size() * sizeof(unsigned short), &indicies[0], GL_STATIC_DRAW);

	glBindVertexArray(NULL);
#endif
}

void CalculateTangents(std::vector<Vertex>& vertices, const std::vector<unsigned short>& indices) {
	unsigned short vertexCount = (unsigned short)vertices.size();
	glm::vec4* tan1 = new glm::vec4[vertexCount * 2];
	glm::vec4* tan2 = tan1 + vertexCount;
	memset(tan1, 0, vertexCount * sizeof(glm::vec4) * 2);

	unsigned short indexCount = (unsigned short)indices.size();
	for (unsigned short a = 0; a < indexCount; a += 3) {
		long i1 = indices[a];
		long i2 = indices[a + 1];
		long i3 = indices[a + 2];

		const glm::vec4& v1 = vertices[i1].position;
		const glm::vec4& v2 = vertices[i2].position;
		const glm::vec4& v3 = vertices[i3].position;

		const glm::vec2& w1 = vertices[i1].texCoord;
		const glm::vec2& w2 = vertices[i2].texCoord;
		const glm::vec2& w3 = vertices[i3].texCoord;

		float x1 = v2.x - v1.x;
		float x2 = v3.x - v1.x;
		float y1 = v2.y - v1.y;
		float y2 = v3.y - v1.y;
		float z1 = v2.z - v1.z;
		float z2 = v3.z - v1.z;

		float s1 = w2.x - w1.x;
		float s2 = w3.x - w1.x;
		float t1 = w2.y - w1.y;
		float t2 = w3.y - w1.y;

		float r = 1.0F / (s1 * t2 - s2 * t1);
		glm::vec4 sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r,
			(t2 * z1 - t1 * z2) * r, 0);
		glm::vec4 tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r,
			(s1 * z2 - s2 * z1) * r, 0);

		tan1[i1] += sdir;
		tan1[i2] += sdir;
		tan1[i3] += sdir;

		tan2[i1] += tdir;
		tan2[i2] += tdir;
		tan2[i3] += tdir;
	}

	for (unsigned short a = 0; a < vertexCount; a++) {
		const glm::vec3& n = glm::vec3(vertices[a].normal);
		const glm::vec3& t = glm::vec3(tan1[a]);

		// Gram-Schmidt orthogonalize
		vertices[a].tangent = glm::vec4(glm::normalize(t - n * glm::dot(n, t)), 0);

		// Calculate handedness (direction of bitangent)
		vertices[a].tangent.w = (glm::dot(glm::cross(glm::vec3(n), glm::vec3(t)), glm::vec3(tan2[a])) < 0.0F) ? 1.0F : -1.0F;

		// calculate bitangent (ignoring for Vertex)
		vertices[a].biTangent = glm::vec4(glm::cross(glm::vec3(vertices[a].normal), glm::vec3(vertices[a].tangent)) * vertices[a].tangent.w, 0);
		vertices[a].tangent.w = 0;
	}

	delete[] tan1;
}

void Model::LoadOBJ(const char* a_fileName, Model**& a_model, int& a_size)
{
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	std::string err;
	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, a_fileName);

	if (!err.empty())
	{
#if defined(_WIN32) && defined(_DEBUG)
		MessageBox(NULL, err.insert(0, "Failed to load obj: ").c_str(), "Error", MB_OK | MB_ICONERROR);
#endif
	}

	if (!ret)
	{
		return;
	}


	a_size = shapes.size();
	a_model = new Model*[a_size];

	for (unsigned int s = 0; s < shapes.size(); ++s)
	{
		std::vector<Vertex> vertices;
		std::vector<unsigned short> indices;

		unsigned int indOff = 0;

		float maxMag = 0.0f;

		for (unsigned int f = 0; f < shapes[s].mesh.num_face_vertices.size(); ++f)
		{
			vertices.reserve(shapes[s].mesh.num_face_vertices.size());
			indices.reserve(shapes[s].mesh.indices.size());

			unsigned int fv = (unsigned int)shapes[s].mesh.num_face_vertices[f];

			for (unsigned int v = 0; v < fv; ++v)
			{
				tinyobj::index_t idx = shapes[s].mesh.indices[indOff + v];

				Vertex vert;

				vert.position.x = attrib.vertices[3 * idx.vertex_index + 0];
				vert.position.y = attrib.vertices[3 * idx.vertex_index + 1];
				vert.position.z = attrib.vertices[3 * idx.vertex_index + 2];
				vert.position.w = 1.0f;

				maxMag = std::max(sqrt(vert.position.x * vert.position.x + vert.position.y * vert.position.y + vert.position.z * vert.position.z), maxMag);

				vert.normal.x = attrib.normals[3 * idx.normal_index + 0];
				vert.normal.y = attrib.normals[3 * idx.normal_index + 1];
				vert.normal.z = attrib.normals[3 * idx.normal_index + 2];

				vert.texCoord.x = attrib.texcoords[2 * idx.texcoord_index + 0];
				vert.texCoord.y = 1.0f - attrib.texcoords[2 * idx.texcoord_index + 1];

				auto iter = std::find(vertices.begin(), vertices.end(), vert);

				if (iter != vertices.end())
				{
					indices.push_back((unsigned short)(iter - vertices.begin()));

					continue;
				}

				vertices.push_back(vert);
				indices.push_back((unsigned short)(vertices.size() - 1));
			}

			indOff += fv;
		}

		CalculateTangents(vertices, indices);

		a_model[s] = new Model();

		a_model[s]->m_boundingRadius = maxMag;
		a_model[s]->m_indicies = (unsigned short)indices.size();

		glGenBuffers(1, &a_model[s]->mp_vertBuffer);
		glGenBuffers(1, &a_model[s]->mp_indiceBuffer);
		glGenVertexArrays(1, &a_model[s]->mp_vertArrayObject);
		glBindVertexArray(a_model[s]->mp_vertArrayObject);

		glBindBuffer(GL_ARRAY_BUFFER, a_model[s]->mp_vertBuffer);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(0, 4, GL_FLOAT, FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, position));
		glVertexAttribPointer(1, 3, GL_FLOAT, FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));
		glVertexAttribPointer(2, 2, GL_FLOAT, FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texCoord));
		glVertexAttribPointer(3, 4, GL_FLOAT, FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, tangent));
		glVertexAttribPointer(4, 4, GL_FLOAT, FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, biTangent));

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, a_model[s]->mp_indiceBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);

		glBindVertexArray(NULL);
	}
}

bool Vertex::operator==(const Vertex & a_vert) const
{
	return (position == a_vert.position) && (normal == a_vert.normal) && (texCoord == a_vert.texCoord);
}
