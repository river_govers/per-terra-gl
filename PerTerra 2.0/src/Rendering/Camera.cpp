#include "Rendering/Camera.h"

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#define DEBGUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBGUG_NEW
#endif 

Camera* Camera::m_primaryCamera = nullptr;
Camera* Camera::m_drawingCamera = nullptr;
std::list<Camera*> Camera::m_cameras = std::list<Camera*>();

#include "Application.h"

#include "gl_core_4_4.h"

Camera::Camera(const float a_fov, const float a_near, const float a_far, const float a_aspectRatio, int a_clearFlags)
{
	m_transform.SetMatrix(Matrix4::Inverse(Matrix4::LookAt({ 0.0f, 2.0f, 5.0f },
														   { 0.0f, 0.0f, 0.0f },
														   { 0.0f, 1.0f, 0.0f })));
	
	m_projection = glm::perspective(a_fov, a_aspectRatio, a_near, a_far);

	m_renderTarget = nullptr;

	m_clearFlags = a_clearFlags;

	// Register the camera to the drawing list
	m_cameras.push_back(this);

	// Set this camera as the main if there is none
	if (!m_primaryCamera)
	{
		m_primaryCamera = this;
	}
}
Camera::~Camera()
{
	// Remove this camera from the drawing list
	m_cameras.remove(this);

	// Set the next camera as the main
	if (m_primaryCamera == this && m_cameras.size() != 0)
	{
		m_primaryCamera = *m_cameras.begin();
	}
}

Transform& Camera::Transform()
{
	return m_transform;
}

glm::mat4 Camera::GetViewProjection() const
{
	return m_projection * m_view;
}

glm::mat4 Camera::Projection() const
{
	return m_projection;
}

glm::mat4 Camera::View() const
{
	return m_view;
}

void Camera::SetRenderTarget(RenderTarget * const a_renderTarget)
{
	m_renderTarget = a_renderTarget;
}

std::list<Camera*>& Camera::GetCameraList()
{
	return m_cameras;
}

void Camera::SetPrimaryCamera(Camera * const a_camera)
{
	m_primaryCamera = a_camera;
}

Camera* Camera::GetPrimaryCamera()
{
	return m_primaryCamera;
}

Camera* Camera::GetDrawingCamera()
{
	return m_drawingCamera;
}
