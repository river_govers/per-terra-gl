#include "Rendering/Shaders.h"

#if _OPEN_GL
#include "gl_core_4_4.h"
#endif

#include <string>

#if defined(_WIN32) && defined(_DEBUG)
#include <Windows.h>
#endif

#include <fstream>
#include <sstream>

PixelShader::PixelShader(const char* a_psSource)
{
#if _OPEN_GL
	mp_pixelShader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(mp_pixelShader, 1, &a_psSource, 0);
	glCompileShader(mp_pixelShader);
#endif
}

PixelShader::~PixelShader()
{
#if _OPEN_GL
	glDeleteShader(mp_pixelShader);
#endif
}

std::string PixelShader::LoadShader(const char * a_fileName)
{
	std::ifstream file = std::ifstream(a_fileName);

	if (file.good())
	{
		std::stringstream stream;

		stream << file.rdbuf();

		file.close();

		std::string shader = stream.str();

		return shader;
	}

#if defined(_WIN32) && defined(_DEBUG)
	MessageBox(NULL, "Failed to open shader file", "Error", MB_OK | MB_ICONERROR);
#endif

	file.close();

	return std::string();
}

GeometryShader::GeometryShader(const char* a_gsSource)
{
	mp_geometryShader = glCreateShader(GL_GEOMETRY_SHADER);

	glShaderSource(mp_geometryShader, 1, &a_gsSource, 0);
	glCompileShader(mp_geometryShader);
}
GeometryShader::~GeometryShader()
{
	glDeleteShader(mp_geometryShader);
}

std::string GeometryShader::LoadShader(const char* a_fileName)
{
	std::ifstream file = std::ifstream(a_fileName);

	if (file.good())
	{
		std::stringstream stream;

		stream << file.rdbuf();

		file.close();

		std::string shader = stream.str();

		return shader;
	}

#if defined(_WIN32) && defined(_DEBUG)
	MessageBox(NULL, "Failed to open shader file", "Error", MB_OK | MB_ICONERROR);
#endif

	file.close();

	return std::string();
}

VertexShader::VertexShader(const char * a_vsSource)
{
#if _OPEN_GL
	mp_vertShader = glCreateShader(GL_VERTEX_SHADER);

	glShaderSource(mp_vertShader, 1, &a_vsSource, 0);
	glCompileShader(mp_vertShader);
#endif
}

VertexShader::~VertexShader()
{
#if _OPEN_GL
	glDeleteShader(mp_vertShader);
#endif
}

std::string VertexShader::LoadShader(const char * a_fileName)
{
	std::ifstream file = std::ifstream(a_fileName);

	if (file.good())
	{
		std::stringstream stream;

		stream << file.rdbuf();
		
		file.close();

		std::string shader = stream.str();

		return shader;
	}

#if defined(_WIN32) && defined(_DEBUG)
	MessageBox(NULL, "Failed to open shader file", "Error", MB_OK | MB_ICONERROR);
#endif

	file.close();

	return std::string();
}

ShaderProgram::ShaderProgram(const PixelShader& a_pixelShader, const VertexShader& a_vertShader)
{
#if _OPEN_GL
	int success;

	mp_programId = glCreateProgram();
	glAttachShader(mp_programId, a_vertShader.mp_vertShader);
	glAttachShader(mp_programId, a_pixelShader.mp_pixelShader);
	glLinkProgram(mp_programId);

	glGetProgramiv(mp_programId, GL_LINK_STATUS, &success);
	if (!success)
	{
#if defined(_WIN32) && defined(_DEBUG)
		int infoLogLength = 0;

		glGetProgramiv(mp_programId, GL_INFO_LOG_LENGTH, &infoLogLength);

		char* infoLog = new char[infoLogLength];

		glGetProgramInfoLog(mp_programId, infoLogLength, NULL, infoLog);

		std::string output = "Failed to link shader program: " + std::string(infoLog);

		delete[] infoLog;

		MessageBox(NULL, output.c_str(), "Open GL Error", MB_OK | MB_ICONERROR);
#endif
	}
#endif
	
	// Stops crashes for some reason
	m_uniformTexture2Ds = std::map<const char*, Texture2DUniform>();
	m_uniformFloats = std::map<const char*, FloatUniform>();

	m_worldUniform = glGetUniformLocation(mp_programId, "world");
	m_viewUniform = glGetUniformLocation(mp_programId, "view");
	m_projectionUniform = glGetUniformLocation(mp_programId, "projection");
	m_lightCountUniform = glGetUniformLocation(mp_programId, "lightCount");
}
ShaderProgram::ShaderProgram(const PixelShader& a_pixelShader, const GeometryShader& a_geometryShader, const VertexShader& a_vertShader)
{
	int success;

	mp_programId = glCreateProgram();
	glAttachShader(mp_programId, a_vertShader.mp_vertShader);
	glAttachShader(mp_programId, a_geometryShader.mp_geometryShader);
	glAttachShader(mp_programId, a_pixelShader.mp_pixelShader);
	glLinkProgram(mp_programId);

	glGetProgramiv(mp_programId, GL_LINK_STATUS, &success);
	if (!success)
	{
#if defined(_WIN32) && defined(_DEBUG)
		int infoLogLength = 0;

		glGetProgramiv(mp_programId, GL_INFO_LOG_LENGTH, &infoLogLength);

		char* infoLog = new char[infoLogLength];

		glGetProgramInfoLog(mp_programId, infoLogLength, NULL, infoLog);

		std::string output = "Failed to link shader program: " + std::string(infoLog);

		delete[] infoLog;

		MessageBox(NULL, output.c_str(), "Open GL Error", MB_OK | MB_ICONERROR);
#endif
	}

	// Stops crashes for some reason
	m_uniformTexture2Ds = std::map<const char*, Texture2DUniform>();
	m_uniformFloats = std::map<const char*, FloatUniform>();

	m_worldUniform = glGetUniformLocation(mp_programId, "world");
	m_viewUniform = glGetUniformLocation(mp_programId, "view");
	m_projectionUniform = glGetUniformLocation(mp_programId, "projection");
	m_lightCountUniform = glGetUniformLocation(mp_programId, "lightCount");
}
ShaderProgram::~ShaderProgram()
{
#if _OPEN_GL
	glDeleteProgram(mp_programId);
#endif
}

void ShaderProgram::SetTexture(const char * a_varName, Texture* const a_texture)
{
	auto iter = m_uniformTexture2Ds.find(a_varName);

	if (iter == m_uniformTexture2Ds.end())
	{
		m_uniformTexture2Ds.insert({ a_varName, { glGetUniformLocation(mp_programId, a_varName), a_texture } });

		return;
	}

	iter->second.texture = a_texture;
}

void ShaderProgram::SetFloat(const char * a_varName, float a_value)
{
	auto iter = m_uniformFloats.find(a_varName);

	if (iter == m_uniformFloats.end())
	{
		m_uniformFloats.insert({ a_varName, { glGetUniformLocation(mp_programId, a_varName), a_value } });

		return;
	}

	iter->second.value = a_value;
}


