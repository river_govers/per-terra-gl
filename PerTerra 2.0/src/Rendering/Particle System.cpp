#include "Rendering/Particle System.h"

#include "gl_core_4_4.h"

#include "TimeG.h"

#include "Rendering\Shaders.h"

void ParticleEmitter::InitBuffers()
{
	if (m_particle.emitter)
	{
		mp_subEmitters = new ParticleEmitter*[m_maxParticles];

		for (unsigned int i = 0; i < m_maxParticles; ++i)
		{
			mp_subEmitters[i] = new ParticleEmitter(*m_particle.emitter);

			mp_subEmitters[i]->m_maxParticles = 0;
		}
	}
	else
	{
		mp_subEmitters = nullptr;
	}

	mp_particles = new ParticleVert[m_maxParticles];

	glGenVertexArrays(1, &mp_vertexArrayObject);
	glBindVertexArray(mp_vertexArrayObject);

	glGenBuffers(1, &mp_vertexBufferObject);

	glBindBuffer(GL_ARRAY_BUFFER, mp_vertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, m_maxParticles * sizeof(ParticleVert), mp_particles, GL_DYNAMIC_DRAW);

	// Position
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleVert), (void*)offsetof(ParticleVert, position));

	// Velocity
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(ParticleVert), (void*)offsetof(ParticleVert, velocity));

	// Color
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleVert), (void*)offsetof(ParticleVert, color));

	// End Color
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(ParticleVert), (void*)offsetof(ParticleVert, endColor));

	// Size
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, sizeof(ParticleVert), (void*)offsetof(ParticleVert, size));

	// End Size
	glEnableVertexAttribArray(5);
	glVertexAttribPointer(5, 1, GL_FLOAT, GL_FALSE, sizeof(ParticleVert), (void*)offsetof(ParticleVert, endSize));

	// Spawn Time
	glEnableVertexAttribArray(6);
	glVertexAttribPointer(6, 1, GL_FLOAT, GL_FALSE, sizeof(ParticleVert), (void*)offsetof(ParticleVert, spawnTime));

	// End Time
	glEnableVertexAttribArray(7);
	glVertexAttribPointer(7, 1, GL_FLOAT, GL_FALSE, sizeof(ParticleVert), (void*)offsetof(ParticleVert, endTime));
}

ParticleEmitter::ParticleEmitter(const ParticleEmitter& a_emitter)
{
	m_shader = a_emitter.m_shader;

	m_emissionTimer = 0.0f;
	m_emissionRate = a_emitter.m_emissionRate;

	m_maxLifeSpan = a_emitter.m_maxLifeSpan;
	m_minLifeSpan = a_emitter.m_minLifeSpan;

	m_maxVelocity = a_emitter.m_maxVelocity;
	m_minVelocity = a_emitter.m_minVelocity;

	m_maxParticles = a_emitter.m_maxParticles;
	m_particleNum = 0;

	m_particle = a_emitter.m_particle;

	InitBuffers();
}
ParticleEmitter::ParticleEmitter(unsigned int a_maxParticles, unsigned int a_emissionRate, float a_lifeTime, float a_velocity, const Particle& a_particle, ShaderProgram& a_shaderProgram)
{
	m_shader = &a_shaderProgram;

	m_emissionTimer = 0.0;
	m_emissionRate = 1.0 / a_emissionRate;

	m_maxLifeSpan = a_lifeTime;
	m_minLifeSpan = a_lifeTime;

	m_maxVelocity = a_velocity;
	m_minVelocity = a_velocity;

	m_maxParticles = a_maxParticles;
	m_particleNum = 0;

	m_particle = a_particle;

	InitBuffers();
}
ParticleEmitter::ParticleEmitter(unsigned int a_maxPartilces, unsigned int a_emissionRate, float a_minLifeTime, float a_maxLifeTime, float a_minVelocity, float a_maxVelocity, const Particle& a_particle, ShaderProgram& a_shaderProgram)
{
	m_shader = &a_shaderProgram;

	m_emissionTimer = 0.0;
	m_emissionRate = 1.0 / a_emissionRate;

	m_maxLifeSpan = a_maxLifeTime;
	m_minLifeSpan = a_minLifeTime;

	m_maxVelocity = a_maxVelocity;
	m_minVelocity = a_minVelocity;

	m_maxParticles = a_maxPartilces;
	m_particleNum = 0;

	m_particle = a_particle;
	/*m_particle.size = 0.15f;*/

	InitBuffers();
}

ParticleEmitter::~ParticleEmitter()
{
	delete[] mp_particles;

	if (mp_subEmitters)
	{
		for (unsigned int i = 0; i < m_maxParticles; ++i)
		{
			if (mp_subEmitters[i])
			{
				mp_subEmitters[i]->m_maxParticles = m_particle.emitter->m_maxParticles;
				delete mp_subEmitters[i];
			}
		}

		delete[] mp_subEmitters;
	}

	glDeleteVertexArrays(1, &mp_vertexArrayObject);
	glDeleteBuffers(1, &mp_vertexBufferObject);
}

template<typename T>
void Swap(T& a_a, T& a_b)
{
	T c = a_a;
	a_a = a_b;
	a_b = c;
}

void ParticleEmitter::Update()
{
	for (unsigned int i = 0; i < m_particleNum; ++i)
	{
		if (TimeG::TimePassed() >= mp_particles[i].endTime)
		{
			Swap<ParticleVert>(mp_particles[i], mp_particles[--m_particleNum]);
			if (m_particle.emitter)
			{
				Swap<ParticleEmitter*>(mp_subEmitters[i], mp_subEmitters[m_particleNum]);
				mp_subEmitters[m_particleNum]->m_maxParticles = 0;
			}
		}
	}

	m_emissionTimer += TimeG::DeltaTime();

	bool set = false;

	while (m_particleNum < m_maxParticles && m_emissionTimer >= m_emissionRate)
	{
		set = true;

		m_emissionTimer -= m_emissionRate;

		ParticleVert& particle = mp_particles[m_particleNum++];

		particle.position = m_transform.GetMatrix()[3];

		particle.velocity.x = (rand() / (float)RAND_MAX) * 2 - 1;
		particle.velocity.y = (rand() / (float)RAND_MAX) * 2 - 1;
		particle.velocity.z = (rand() / (float)RAND_MAX) * 2 - 1;
		particle.velocity = glm::normalize(particle.velocity) * ((rand() / (float)RAND_MAX) * (m_maxVelocity - m_minVelocity) + m_minVelocity);

		particle.spawnTime = (float)TimeG::TimePassed();
		particle.endTime = (float)TimeG::TimePassed() + ((rand() / (float)RAND_MAX) * (m_maxLifeSpan - m_minLifeSpan) + m_minLifeSpan);

		particle.color = m_particle.color;
		particle.endColor = m_particle.endColor;
		particle.size = m_particle.size;
		particle.endSize = m_particle.endSize;

		if (m_particle.emitter)
		{
			mp_subEmitters[m_particleNum - 1]->m_maxParticles = m_particle.emitter->m_maxParticles;
		}
	}

	if (set)
	{
		glBufferSubData(GL_ARRAY_BUFFER, 0, m_particleNum - 1, mp_particles);
	}

	if (m_particle.emitter)
	{
		for (unsigned int i = 0; i < m_maxParticles; ++i)
		{
			if (i < m_particleNum)
			{
				mp_subEmitters[i]->Transform().SetPosition(mp_particles[i].position.xyz + mp_particles[i].velocity * (TimeG::TimePassed() - mp_particles[i].spawnTime));
			}
			mp_subEmitters[i]->Update();
		}

		/*for (unsigned int i = 0; i < m_particleNum; ++i)
		{
			mp_subEmitters[i]->Transform().SetPosition(mp_particles[i].position.xyz + mp_particles[i].velocity * (TimeG::TimePassed() - mp_particles[i].spawnTime));
			mp_subEmitters[i]->Update();
		}*/

		/*if (m_particleNum)
		{
			mp_subEmitters[0]->Transform().SetPosition(mp_particles[1].position.xyz + mp_particles[1].velocity * (TimeG::TimePassed() - mp_particles[1].spawnTime));
			mp_subEmitters[0]->Update();
		}*/
	}
}

void ParticleEmitter::Clear()
{
	m_particleNum = 0;
}

void ParticleEmitter::SetMinVelocity(float a_velocity)
{
	m_minVelocity = a_velocity;
}
void ParticleEmitter::SetMaxVeloctiy(float a_velocity)
{
	m_maxVelocity = a_velocity;
}

void ParticleEmitter::SetMinLifeSpan(float a_lifeSpan)
{
	m_minLifeSpan = a_lifeSpan;
}
void ParticleEmitter::SetMaxLifeSpan(float a_lifeSpan)
{
	m_maxLifeSpan = a_lifeSpan;
}

void ParticleEmitter::SetEmmisionRate(unsigned int a_emissionRate)
{
	m_emissionRate = 1.0 / a_emissionRate;
}

void ParticleEmitter::SetParticle(const Particle& a_particle)
{
	m_particle = a_particle;
}

void ParticleEmitter::SetShader(ShaderProgram & a_shader)
{
	m_shader = &a_shader;
}

Transform& ParticleEmitter::Transform()
{
	return m_transform;
}
