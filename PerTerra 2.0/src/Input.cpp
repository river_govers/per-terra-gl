#include "Input.h"

#ifdef XINPUT
#pragma comment (lib, "XInput9_1_0.lib")
#endif

#include <cmath>

InputManager* InputManager::mp_inputManager = nullptr;

#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))

InputManager::InputManager()
{
#ifdef XINPUT
	m_leftDeadzone = XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE;
	m_rightDeadzone = XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE;
	m_triggerDeadzone = XINPUT_GAMEPAD_TRIGGER_THRESHOLD;
#endif

	for (int i = 0; i < MAX_GAMEPADS; ++i)
	{
		m_gamePads[i] = Controller();
	}
}
InputManager::~InputManager()
{

}

void InputManager::Update()
{
#ifdef XINPUT
	for (int i = 0; i < MAX_GAMEPADS; ++i)
	{
		ZeroMemory(&mp_inputManager->m_gamePads[i].gamepadState, sizeof(XINPUT_STATE));
		mp_inputManager->m_gamePads[i].state = XInputGetState((DWORD)i, &mp_inputManager->m_gamePads[i].gamepadState) == ERROR_SUCCESS;
	}
#else
	for (int i = 0; i < MAX_GAMEPADS; ++i)
	{
		Controller& controller = mp_inputManager->m_gamePads[i];

		controller.state = glfwJoystickPresent(i) == TRUE;
		controller.name = glfwGetJoystickName(i);
	}
#endif
}

float InputManager::GetGamepadAxis(int a_gamePad, e_Axis a_axis)
{
	if (mp_inputManager->m_gamePads[a_gamePad].state)
	{
#ifdef XINPUT
		switch (a_axis)
		{
		case e_Axis::LeftX:
		case e_Axis::LeftY:
		{
			XINPUT_STATE& state = mp_inputManager->m_gamePads[a_gamePad].gamepadState;

			float lx = state.Gamepad.sThumbLX;
			float ly = state.Gamepad.sThumbLY;

			float mag = sqrt(lx * lx + ly * ly);

			if (mag > mp_inputManager->m_leftDeadzone)
			{
				switch (a_axis)
				{
				case e_Axis::LeftX:
				{
					return lx / max(lx, GAMEPAD_THUMBCAP);
				}
				case e_Axis::LeftY:
				{
					return ly / max(ly, GAMEPAD_THUMBCAP);
				}
				}
			}

			return 0.0f;
		}
		case e_Axis::RightX:
		case e_Axis::RightY:
		{
			XINPUT_STATE& state = mp_inputManager->m_gamePads[a_gamePad].gamepadState;

			float rx = state.Gamepad.sThumbRX;
			float ry = state.Gamepad.sThumbRY;

			float mag = sqrt(rx * rx + ry * ry);

			if (mag > mp_inputManager->m_rightDeadzone)
			{
				switch (a_axis)
				{
				case e_Axis::RightX:
				{
					return rx / max(rx, GAMEPAD_THUMBCAP);
				}
				case e_Axis::RightY:
				{
					return ry / max(ry, GAMEPAD_THUMBCAP);
				}
				}
			}

			return 0.0f;
		}
		case e_Axis::RightTrigger:
		{
			XINPUT_STATE& state = mp_inputManager->m_gamePads[a_gamePad].gamepadState;

			if (state.Gamepad.bRightTrigger > mp_inputManager->m_triggerDeadzone)
			{
				return (float)state.Gamepad.bRightTrigger / GAMEPAD_TRIGGERCAP;
			}

			return 0.0f;
		}
		case e_Axis::LeftTrigger:
		{
			XINPUT_STATE& state = mp_inputManager->m_gamePads[a_gamePad].gamepadState;

			if (state.Gamepad.bLeftTrigger > mp_inputManager->m_triggerDeadzone)
			{
				return (float)state.Gamepad.bLeftTrigger / GAMEPAD_TRIGGERCAP;
			}

			return 0.0f;
		}
		}
#else
		int axesCount;
		const float* axes = glfwGetJoystickAxes(a_gamePad, &axesCount);

		if (a_axis == e_Axis::LeftTrigger || a_axis == e_Axis::RightTrigger)
		{
			return (axes[(int)a_axis] + 1.0f) * 0.5f;
		}

		return axes[(int)a_axis];
#endif
	}

	return 0.0f;
}
bool InputManager::GetGamepadButtonDown(e_GamePadNum a_gamePad, e_GamepadButton a_button)
{
	for (int i = 0; i < MAX_GAMEPADS; ++i)
	{
		if (mp_inputManager->m_gamePads[i].state)
		{
			bool valid;
			switch (i)
			{
			case 0:
			{
				valid = (unsigned char)a_gamePad & 0b00000001;

				break;
			}
			case 1:
			{
				valid = (unsigned char)a_gamePad & 0b00000010;

				break;
			}
			case 2:
			{
				valid = (unsigned char)a_gamePad & 0b00000100;

				break;
			}
			case 3:
			{
				valid = (unsigned char)a_gamePad & 0b00001000;

				break;
			}
			case 4:
			{
				valid = (unsigned char)a_gamePad & 0b00010000;
				break;
			}
			case 5:
			{
				valid = (unsigned char)a_gamePad & 0b00100000;

				break;
			}
			case 6:
			{
				valid = (unsigned char)a_gamePad & 0b01000000;

				break;
			}
			case 7:
			{
				valid = (unsigned char)a_gamePad & 0100000000;

				break;
			}
			}

			if (valid)
			{
				if (mp_inputManager->m_gamePads[i].gamepadState.Gamepad.wButtons & (WORD)a_button)
				{
					return true;
				}
			}
		}
	}

	return false;
}

bool InputManager::GetGamepadButtonDown(int a_gamePad, e_GamepadButton a_button)
{
	if (mp_inputManager->m_gamePads[a_gamePad].state)
	{
		return mp_inputManager->m_gamePads[a_gamePad].gamepadState.Gamepad.wButtons & (WORD)a_button;
	}

	return false;
}

void InputManager::SetLeftDeadzone(unsigned short a_mag)
{
	mp_inputManager->m_leftDeadzone = a_mag;
}
void InputManager::SetRightDeadzone(unsigned short a_mag)
{
	mp_inputManager->m_rightDeadzone = a_mag;
}
void InputManager::SetTriggerDeadzone(unsigned short a_mag)
{
	mp_inputManager->m_triggerDeadzone = a_mag;
}
