#include "Frustrum.h"

#include "Rendering\Camera.h"

#include "Maths\Maths.h"

Frustrum::Frustrum(const Camera& a_camera)
{
	glm::mat4 viewProjection = a_camera.GetViewProjection();

	for (int i = 0; i < 3; ++i)
	{
		m_planes[i * 2] = { viewProjection[0][3] - viewProjection[0][i],
							viewProjection[1][3] - viewProjection[1][i],
							viewProjection[2][3] - viewProjection[2][i],
							viewProjection[3][3] - viewProjection[3][i] };
		m_planes[i * 2] *= /*glm::inversesqrt(m_planes[i * 2].x * m_planes[i * 2].x + m_planes[i * 2].y * m_planes[i * 2].y + m_planes[i * 2].z * m_planes[i * 2].z)*/ 
							InvSqrt(m_planes[i * 2].x * m_planes[i * 2].x + m_planes[i * 2].y * m_planes[i * 2].y + m_planes[i * 2].z * m_planes[i * 2].z);

		m_planes[i * 2 + 1] = { viewProjection[0][3] + viewProjection[0][i],
								viewProjection[1][3] + viewProjection[1][i],
								viewProjection[2][3] + viewProjection[2][i],
								viewProjection[3][3] + viewProjection[3][i] };
		m_planes[i * 2 + 1] *= /*glm::inversesqrt(m_planes[i * 2 + 1].x * m_planes[i * 2].x + m_planes[i * 2 + 1].y * m_planes[i * 2 + 1].y + m_planes[i * 2 + 1].z * m_planes[i * 2 + 1].z)*/
								InvSqrt(m_planes[i * 2].x * m_planes[i * 2].x + m_planes[i * 2].y * m_planes[i * 2].y + m_planes[i * 2].z * m_planes[i * 2].z);
	}
}

bool Frustrum::CompareSphere(const glm::vec3 & a_pos, float a_radius)
{
	for (int i = 0; i < 6; ++i)
	{
		float d = glm::dot(glm::vec3(m_planes[i]), a_pos) + m_planes[i].w;

		if (d < -a_radius)
		{
			return false;
		}
	}

	return true;
}
