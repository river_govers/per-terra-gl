#include "..\include\TimeG.h"

TimeG* TimeG::m_time = nullptr;

double TimeG::DeltaTime()
{
	return m_time->m_deltaTime * m_time->m_timeScale;
}

double TimeG::UnscaledDeltaTime()
{
	return m_time->m_deltaTime;
}

double TimeG::TimePassed()
{
	return m_time->m_timePassed;
}

void TimeG::SetTimeScale(double a_timeScale)
{
	m_time->m_timeScale = a_timeScale;
}
