#include "Maths/Maths.h"

#if _DIRECT_X
Vector3::Vector3(const DirectX::XMVECTOR& a_vector)
{
	m_dxVector = a_vector;
}
#endif

#if _OPEN_GL
Vector3::Vector3(const glm::vec3 & a_vector)
{
	m_glVector = a_vector;
}
#endif

Vector3::Vector3()
{
#if _DIRECT_X
	DirectX::XMVectorSetX(m_dxVector, 0.0f);
	DirectX::XMVectorSetY(m_dxVector, 0.0f);
	DirectX::XMVectorSetZ(m_dxVector, 0.0f);
#endif

#if _OPEN_GL
	m_glVector = glm::vec3(0.0f);
#endif
}

Vector3::Vector3(float a_x, float a_y, float a_z)
{
#if _DIRECT_X
	DirectX::XMVectorSetX(m_dxVector, a_x);
	DirectX::XMVectorSetY(m_dxVector, a_y);
	DirectX::XMVectorSetZ(m_dxVector, a_z);
#endif

#if _OPEN_GL
	m_glVector = glm::vec3(a_x, a_y, a_z);
#endif
}

float Vector3::GetX() const
{
#if _DIRECT_X
	return DirectX::XMVectorGetX(m_dxVector);
#endif

#if _OPEN_GL
	return m_glVector.x;
#endif
}

float Vector3::GetY() const
{
#if _DIRECT_X
	return DirectX::XMVectorGetY(m_dxVector);
#endif

#if _OPEN_GL
	return m_glVector.y;
#endif
}

float Vector3::GetZ() const
{
#if _DIRECT_X
	return DirectX::XMVectorGetX(m_dxVector);
#endif
#if _OPEN_GL
	return m_glVector.z;
#endif

}

float Vector3::GetIndex(int a_index) const
{
#if _DIRECT_X
	return DirectX::XMVectorGetByIndex(m_dxVector, a_index);
#endif

#if _OPEN_GL
	return m_glVector[a_index];
#endif
}

void Vector3::SetX(float a_x)
{
#if _DIRECT_X
	DirectX::XMVectorSetX(m_dxVector, a_x);
#endif // _DIRECT_X

#if _OPEN_GL
	m_glVector.x = a_x;
#endif
}
void Vector3::SetY(float a_y)
{
#if _DIRECT_X
	DirectX::XMVectorSetY(m_dxVector, a_y);
#endif // _DIRECT_X

#if _OPEN_GL
	m_glVector.y = a_y;
#endif
}

void Vector3::SetZ(float a_z)
{
#if _DIRECT_X
	DirectX::XMVectorSetZ(m_dxVector, a_z);
#endif // _DIRECT_X

#if _OPEN_GL
	m_glVector.z = a_z;
#endif
}
void Vector3::SetIndex(int a_index, float a_value)
{
#if _DIRECT_X
	DirectX::XMVectorSetByIndex(m_dxVector, a_value, a_index);
#endif

#if _OPEN_GL
	m_glVector[a_index] = a_value;
#endif
}

Vector3 Vector3::operator=(const Vector3& a_vector)
{
#if _DIRECT_X
	return m_dxVector = a_vector.m_dxVector;
#endif // _DIRECT_X

#if _OPEN_GL
	return m_glVector = a_vector.m_glVector;
#endif
}

Vector3 Vector3::operator*(float a_scalar) const
{
#if _DIRECT_X
	using namespace DirectX;
	return m_dxVector * a_scalar;
#endif // _DIRECT_X

#if _OPEN_GL
	return m_glVector * a_scalar;
#endif
}
Vector3 Vector3::operator/(float a_scalar) const
{
#if _DIRECT_X
	using namespace DirectX;
	return m_dxVector / a_scalar;
#endif

#if _OPEN_GL
	return m_glVector / a_scalar;
#endif
}
Vector3 Vector3::operator+(const Vector3& a_vector) const
{
#if _DIRECT_X
	return Vector3(GetX() + a_vector.GetX(), GetY() + a_vector.GetY(), GetZ() + a_vector.GetZ());
#endif

#if _OPEN_GL
	return m_glVector + a_vector.m_glVector;
#endif // _OPEN_GL
}
Vector3 Vector3::operator-(const Vector3& a_vector) const
{
#if _DIRECT_X
	return Vector3(GetX() - a_vector.GetX(), GetY() - a_vector.GetY(), GetZ() - a_vector.GetZ());
#endif

#if _OPEN_GL
	return m_glVector - a_vector.m_glVector;
#endif
}

Vector3 Vector3::operator*=(float a_scalar)
{
	return *this = *this * a_scalar;
}
Vector3 Vector3::operator/=(float a_scalar)
{
	return *this = *this / a_scalar;
}
Vector3 Vector3::operator+=(const Vector3 & a_vector)
{
	return *this = *this + a_vector;
}
Vector3 Vector3::operator-=(const Vector3 & a_vector)
{
	return *this = *this - a_vector;
}

Vector3 Vector3::Normalized() const
{
#if _DIRECT_X
	return Vector3(DirectX::XMVector3Normalize(m_dxVector));
#endif // _DIRECT_X

#if _OPEN_GL
	return Vector3(glm::normalize(m_glVector));
#endif
}

void Vector3::Normalize()
{
	*this = Normalized();
}

Vector3 Vector3::Cross(const Vector3 & a_vectorA, const Vector3 & a_vectorB)
{
	return{ a_vectorA.GetY() * a_vectorB.GetZ() - a_vectorA.GetZ() * a_vectorB.GetY(),
			a_vectorA.GetZ() * a_vectorB.GetX() - a_vectorA.GetX() * a_vectorB.GetZ(),
			a_vectorA.GetX() * a_vectorB.GetY() - a_vectorA.GetY() * a_vectorB.GetX() };
}

#if _DIRECT_X
Matrix4::Matrix4(const DirectX::XMMATRIX& a_matrix)
{
	m_dxMatrix = a_matrix;
}
void * Matrix4::operator new(size_t i)
{
	return _mm_malloc(i, 16);
}
void Matrix4::operator delete(void * p)
{
	_mm_free(p);
}
#endif

#if _OPEN_GL
Matrix4::Matrix4(const glm::mat4 & a_matrix) : Matrix4(a_matrix[0][0], a_matrix[0][1], a_matrix[0][2], a_matrix[0][3],
													   a_matrix[1][0], a_matrix[1][1], a_matrix[1][2], a_matrix[1][3],
													   a_matrix[2][0], a_matrix[2][1], a_matrix[2][2], a_matrix[2][3],
													   a_matrix[3][0], a_matrix[3][1], a_matrix[3][2], a_matrix[3][3])
{
	
}
Matrix4::operator glm::mat4() const
{
	return { m_mat4x4[0][0], m_mat4x4[0][1], m_mat4x4[0][2], m_mat4x4[0][3],
			 m_mat4x4[1][0], m_mat4x4[1][1], m_mat4x4[1][2], m_mat4x4[1][3],
			 m_mat4x4[2][0], m_mat4x4[2][1], m_mat4x4[2][2], m_mat4x4[2][3],
			 m_mat4x4[3][0], m_mat4x4[3][1], m_mat4x4[3][2], m_mat4x4[3][3] };
}
#endif

Matrix4::Matrix4()
{
	m_matix[0] = 1.0f;  m_matix[1] = 0.0f;  m_matix[2] = 0.0f;  m_matix[3] = 0.0f;
	m_matix[4] = 0.0f;  m_matix[5] = 1.0f;  m_matix[6] = 0.0f;  m_matix[7] = 0.0f;
	m_matix[8] = 0.0f;  m_matix[9] = 0.0f;  m_matix[10] = 1.0f; m_matix[11] = 0.0f;
	m_matix[12] = 0.0f; m_matix[13] = 0.0f; m_matix[14] = 0.0f; m_matix[15] = 1.0f;
}
Matrix4::Matrix4(float a_00, float a_01, float a_02, float a_03, float a_10, float a_11, float a_12, float a_13, float a_20, float a_21, float a_22, float a_23, float a_30, float a_31, float a_32, float a_33)
{
	m_mat4x4[0][0] = a_00; m_mat4x4[1][0] = a_01; m_mat4x4[2][0] = a_02; m_mat4x4[3][0] = a_03;
	m_mat4x4[0][1] = a_10; m_mat4x4[1][1] = a_11; m_mat4x4[2][1] = a_12; m_mat4x4[3][1] = a_13;
	m_mat4x4[0][2] = a_20; m_mat4x4[1][2] = a_21; m_mat4x4[2][2] = a_22; m_mat4x4[3][2] = a_23;
	m_mat4x4[0][3] = a_30; m_mat4x4[1][3] = a_31; m_mat4x4[2][3] = a_32; m_mat4x4[3][3] = a_33;
}

float Matrix4::GetIndex(int a_index) const
{
	return m_matix[a_index];
}
float Matrix4::GetIndex(int a_x, int a_y) const
{
	return m_mat4x4[a_y][a_x];
}

void Matrix4::SetIndex(int a_index, float a_value)
{
	m_matix[a_index] = a_value;
}
void Matrix4::SetIndex(int a_x, int a_y, float a_value)
{
	m_mat4x4[a_y][a_x] = a_value;
}

Matrix4& Matrix4::operator=(const Matrix4 & a_matrix)
{
	for (int i = 0; i < 16; ++i)
	{
		m_matix[i] = a_matrix.m_matix[i];
	}

	return *this;
}

Matrix4 Matrix4::operator*(const Matrix4& a_matrix) const
{
	Matrix4 temp = Matrix4(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			for (int k = 0; k < 4; ++k)
			{
				temp.m_mat4x4[i][j] += m_mat4x4[i][k] * a_matrix.m_mat4x4[k][j];
			}
		}
	}

	return temp;
}
Matrix4 Matrix4::operator+(const Matrix4 & a_matrix) const
{
	return { m_matix[0] + a_matrix.m_matix[0], m_matix[1] + a_matrix.m_matix[1], m_matix[2] + a_matrix.m_matix[2], m_matix[3] + a_matrix.m_matix[3],
			 m_matix[4] + a_matrix.m_matix[4], m_matix[5] + a_matrix.m_matix[5], m_matix[6] + a_matrix.m_matix[6], m_matix[7] + a_matrix.m_matix[7],
			 m_matix[8] + a_matrix.m_matix[8], m_matix[9] + a_matrix.m_matix[9], m_matix[10] + a_matrix.m_matix[10], m_matix[11] + a_matrix.m_matix[11],
			 m_matix[12] + a_matrix.m_matix[12], m_matix[13] + a_matrix.m_matix[13], m_matix[14] + a_matrix.m_matix[14], m_matix[15] + a_matrix.m_matix[15] };
}
Matrix4 Matrix4::operator-(const Matrix4 & a_matrix) const
{
	return { m_matix[0] - a_matrix.m_matix[0], m_matix[1] - a_matrix.m_matix[1], m_matix[2] - a_matrix.m_matix[2], m_matix[3] - a_matrix.m_matix[3],
			 m_matix[4] - a_matrix.m_matix[4], m_matix[5] - a_matrix.m_matix[5], m_matix[6] - a_matrix.m_matix[6], m_matix[7] - a_matrix.m_matix[7],
			 m_matix[8] - a_matrix.m_matix[8], m_matix[9] - a_matrix.m_matix[9], m_matix[10] - a_matrix.m_matix[10], m_matix[11] - a_matrix.m_matix[11],
			 m_matix[12] - a_matrix.m_matix[12], m_matix[13] - a_matrix.m_matix[13], m_matix[14] - a_matrix.m_matix[14], m_matix[15] - a_matrix.m_matix[15] };
}

Matrix4& Matrix4::operator*=(const Matrix4 & a_matrix)
{
	return *this = *this * a_matrix;
}
Matrix4& Matrix4::operator+=(const Matrix4 & a_matrix)
{
	return *this = *this + a_matrix;
}
Matrix4& Matrix4::operator-=(const Matrix4 & a_matrix)
{
	return *this = *this - a_matrix;
}

Matrix4 Matrix4::LookAt(const Vector3 & a_eye, const Vector3 & a_at, const Vector3 & a_up)
{
#if _DIRECT_X
	return DirectX::XMMatrixLookAtRH(a_eye.m_dxVector, a_at.m_dxVector, a_up.m_dxVector);
#endif

#if _OPEN_GL
	return glm::lookAt(a_eye.m_glVector, a_at.m_glVector, a_up.m_glVector);
#endif // _OPEN_GL
}
Matrix4 Matrix4::Perspective(float a_fov, float a_aspectRatio, float a_near, float a_far)
{
#if _DIRECT_X
	return DirectX::XMMatrixPerspectiveFovRH(a_fov, a_aspectRatio, a_near, a_far);
#endif

#if _OPEN_GL
	return glm::perspective(a_fov, a_aspectRatio, a_near, a_far);
#endif
}

Matrix4 Matrix4::Inverse(const Matrix4 & a_matrix)
{
#if _OPEN_GL
	return glm::inverse((glm::mat4)a_matrix);
#endif // _OPEN_GL
}

void Matrix4::Decompose(const Matrix4 & a_matrix, Vector3 * a_translation, Vector3 * a_rotation, Vector3 * a_scale)
{
#if _OPEN_GL
	glm::vec3 scew;
	glm::vec4 perspective;
	glm::quat quaternion;

	glm::decompose((glm::mat4)a_matrix, a_scale->m_glVector, quaternion, a_translation->m_glVector, scew, perspective);

	a_rotation->m_glVector = glm::eulerAngles(quaternion);
#endif

}

int Power2Roundup(int a_x)
{
	if (a_x < 0)
	{
		return 0;
	}
	--a_x;
	a_x |= a_x >> 1;
	a_x |= a_x >> 2;
	a_x |= a_x >> 4;
	a_x |= a_x >> 8;
	a_x |= a_x >> 16;

	return a_x + 1;
}

float InvSqrt(float a_num)
{
	float xHalf = 0.5f * a_num;
	int i = *(int*)&a_num;
	i = 0x5f375a86 - (i >> 1);
	a_num = *(float*)&i;
	return a_num * (1.5f - xHalf * a_num * a_num);
}
