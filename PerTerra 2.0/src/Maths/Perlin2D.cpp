#include "Maths/Perlin2D.h"

#include <random>

#include "Maths/Maths.h"

double LerpD(double a_min, double a_max, double a_t)
{
	return a_min + a_t * (a_max - a_min);
}

PerlinNoise2D::PerlinNoise2D(int a_seed, float a_amplitude, float a_frequency)
{
	m_amplitude = a_amplitude;
	m_frequency = a_frequency;

	int* pemutation = new int[256];

	for (int i = 0; i < 256; i++)
	{
		pemutation[i] = i;
	}

	std::seed_seq seed = { a_seed };
	std::mt19937 engine = std::mt19937(seed);

	for (int i = 0; i < 384; i++)
	{
		int inda = engine() % 256;
		int indb = engine() % 256;

		int temp = pemutation[indb];
		pemutation[indb] = pemutation[inda];
		pemutation[inda] = temp;
	}

	m_hash = new int[512];

	// out of range protection
	for (int i = 0; i < 256; i++)
	{
		m_hash[i] = pemutation[i % 256];
	}

	delete pemutation;
}

PerlinNoise2D::~PerlinNoise2D()
{
	delete m_hash;
}

double PerlinNoise2D::Fade(double a_t) const
{
	return a_t * a_t * a_t * (a_t * (a_t * 6 - 15) + 10);
}

double PerlinNoise2D::Gradient(int a_hash, double a_x, double a_y) const
{
	return (((a_hash & 0xF) == 0x1) ? a_x : -a_x) + (((a_hash & 0xF) == 0x2) ? a_y : a_y);
}

double PerlinNoise2D::NoiseNonVar(double a_x, double a_y, double a_amplitude, double a_frequency) const
{
	a_x *= a_frequency;
	a_y *= a_frequency;

	int xi = (int)a_x & 255;
	int yi = (int)a_y & 255;
	double xf = a_x - (int)a_x;
	double yf = a_y - (int)a_y;

	double u = Fade(xf);
	double v = Fade(yf);

	int aa = m_hash[m_hash[xi] + yi];
	int ab = m_hash[m_hash[xi + 1] + yi];
	int ba = m_hash[m_hash[xi] + yi + 1];
	int bb = m_hash[m_hash[xi + 1] + yi + 1];

	double x1 = LerpD(Gradient(aa, xf, yf), Gradient(ba, xf - 1, yf), u);
	double x2 = LerpD(Gradient(ab, xf, yf - 1), Gradient(bb, xf - 1, yf - 1), u);

	return ((LerpD(x1, x2, v) + 1) / 2) * a_amplitude;
}

double PerlinNoise2D::Noise(double a_x, double a_y) const
{
	a_x *= m_frequency;
	a_y *= m_frequency;

	int xi = (int)a_x & 255;
	int yi = (int)a_y & 255;
	double xf = a_x - (int)a_x;
	double yf = a_y - (int)a_y;

	double u = Fade(xf);
	double v = Fade(yf);

	int aa = m_hash[m_hash[xi]	   + yi];
	int ab = m_hash[m_hash[xi + 1] + yi];
	int ba = m_hash[m_hash[xi]	   + yi + 1];
	int bb = m_hash[m_hash[xi + 1] + yi + 1];

	double x1 = LerpD(Gradient(aa, xf, yf), Gradient(ba, xf - 1, yf), u);
	double x2 = LerpD(Gradient(ab, xf, yf - 1), Gradient(bb, xf - 1, yf - 1), u);

	return ((LerpD(x1, x2, v) + 1) / 2) * m_amplitude;
}

double PerlinNoise2D::OctaveNoise(double a_x, double a_y, int a_octaves, double persistence) const
{
	double total = 0.0;
	double frequency = m_frequency;
	double amplitude = 1.0;
	double maxValue = 0.0;

	for (int i = 0; i < a_octaves; i++)
	{
		total += NoiseNonVar(a_x, a_y, amplitude, frequency);

		maxValue += amplitude;

		amplitude *= persistence;
		frequency *= 2.0;
	}

	return (total / maxValue) * m_amplitude;
}
