#include "Maths/Transform.h"

Transform::Transform()
{
	m_matrix = glm::mat4(1.0f);
}

Transform::~Transform()
{
	for (auto iter = m_children.begin(); iter != m_children.end(); ++iter)
	{
		(*iter)->m_parent = nullptr;
	}
}

void Transform::SetPosition(const Vector3 & a_position)
{
	/*m_position = a_position;*/

	m_matrix[3] = { a_position.GetX(), a_position.GetY(), a_position.GetZ(), 1.0f };
}

void Transform::Translate(const Vector3 & a_translation)
{
	/*m_position += a_translation;*/

	m_matrix[3] += glm::vec4{ a_translation.GetX(), a_translation.GetY(), a_translation.GetZ(), 0.0f };
}

void Transform::SetRotation(const Vector3& a_euler)
{
	/*m_euler = a_euler;

	m_up = { sinf(a_euler.GetZ()), cosf(a_euler.GetZ()), 0.0f };

	m_forward = { sinf(a_euler.GetY()) * cosf(a_euler.GetX()), -sinf(a_euler.GetX()), cosf(a_euler.GetY()) * cosf(a_euler.GetX()) };

	m_right = Vector3::Cross(m_up, m_forward);
	m_up = Vector3::Cross(m_forward, m_right);

	m_forward.Normalize();
	m_up.Normalize();
	m_right.Normalize();*/
}
void Transform::Rotate(const Vector3& a_euler)
{
	/*SetRotation(m_euler + a_euler);*/

	m_matrix = glm::rotate(m_matrix, a_euler.GetX(), -glm::vec3(1.0f, 0.0f, 0.0f));
	m_matrix = glm::rotate(m_matrix, a_euler.GetY(), -glm::vec3(0.0f, 1.0f, 0.0f));
	m_matrix = glm::rotate(m_matrix, a_euler.GetZ(), -glm::vec3(0.0f, 0.0f, 1.0f));
}

/*Matrix4*/ glm::mat4 Transform::GetMatrix() const
{
	return m_matrix;

	/*return Matrix4(m_scale.GetX(), 0.0f,           0.0f,		   0.0f,
				   0.0f,		   m_scale.GetY(), 0.0f,		   0.0f,
				   0.0f,		   0.0f,		   m_scale.GetZ(), 0.0f,
				   0.0f,		   0.0f,		   0.0f,		   1.0f)
		*
		Matrix4(1.0f, 0.0f, 0.0f, m_position.GetX(),
				0.0f, 1.0f, 0.0f, m_position.GetY(),
				0.0f, 0.0f, 1.0f, m_position.GetZ(),
				0.0f, 0.0f, 0.0f, 1.0f)
		*
		Matrix4(m_right.GetX(), m_up.GetX(), m_forward.GetX(), 0.0f,
				m_right.GetY(), m_up.GetY(), m_forward.GetY(), 0.0f,
				m_right.GetZ(), m_up.GetZ(), m_forward.GetZ(), 0.0f,
				0.0f,			0.0f,		 0.0f,			   1.0f)
		;*/

		//return Matrix4(m_scale.GetX(), 0.0f,		   0.0f,		   0.0f,
		//			   0.0f,		   m_scale.GetY(), 0.0f,		   0.0f,
		//			   0.0f,		   0.0f,		   m_scale.GetZ(), 0.0f,
		//			   0.0f,		   0.0f,		   0.0f,		   1.0f)
		//	   *
		//	   Matrix4(m_right.GetX(),	  m_right.GetY(),	m_right.GetZ(),	  0.0f,
		//			    m_up.GetX(),	  m_up.GetY(),		m_up.GetZ(),	  0.0f,
		//			    m_forward.GetX(), m_forward.GetY(), m_forward.GetZ(), 0.0f,
		//			    0.0f,			  0.0f,				0.0f,			  1.0f)
		//	   *
		//	   Matrix4(1.0f,			  0.0f,              0.0f,              0.0f,
		//			   0.0f,			  1.0f,              0.0f,              0.0f,
		//			   0.0f,			  0.0f,              1.0f,              0.0f,
		//			   m_position.GetX(), m_position.GetY(), m_position.GetZ(), 1.0f)
		//	;

}

void Transform::SetMatrix(const Matrix4& a_matrix)
{
	/*Matrix4::Decompose(a_matrix, &m_position, &m_euler, &m_scale);

	m_up = { sinf(m_euler.GetZ()), cosf(m_euler.GetZ()), 0.0f };
	m_forward = { sinf(m_euler.GetY()) * cosf(m_euler.GetX()), -sinf(m_euler.GetX()), cosf(m_euler.GetY()) * cosf(m_euler.GetX()) };
	m_right = Vector3::Cross(m_up, m_forward);
	m_up = Vector3::Cross(m_forward, m_right);

	m_forward.Normalize();
	m_right.Normalize();
	m_up.Normalize();*/

	m_matrix = (glm::mat4)a_matrix;
}

Vector3 Transform::Forward() const
{
	/*return m_forward;*/

	return { m_matrix[2].x, m_matrix[2].y, m_matrix[2].z };
}

Vector3 Transform::Up() const
{
	/*return m_up;*/

	return{ m_matrix[1].x, m_matrix[1].y, m_matrix[1].z };
}

Vector3 Transform::Right() const
{
	/*return m_right;*/

	return{ m_matrix[0].x, m_matrix[0].y, m_matrix[0].z };
}
